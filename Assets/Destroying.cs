﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exploder
{
    public class Destroying : MonoBehaviour
    {
        public AudioClip Crash1;
        public AudioSource _audio;

        public ExploderObject _exploder;
        public GameObject[] ObjectsForDestroy;

        public GameObject VFX;
        public Transform VFXSpawn;
        // Use this for initialization
        void Start()
        {
            _exploder = _exploder.GetComponent<ExploderObject>();
        }


        public void Destroy1()
        {
            // Instantiate(VFX, VFXSpawn.position, VFXSpawn.rotation);
            _audio.PlayOneShot(Crash1);
            _exploder.CrackObject(ObjectsForDestroy[0]);
            _exploder.ExplodeObject(ObjectsForDestroy[0]);
        }

        public void Destroy2()
        {
            Instantiate(VFX, transform.position, transform.rotation);
            _exploder.CrackObject(ObjectsForDestroy[1]);
            _exploder.ExplodeObject(ObjectsForDestroy[1]);
        }

    }
}
