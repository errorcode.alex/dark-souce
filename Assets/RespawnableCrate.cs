﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnableCrate : MonoBehaviour {

    public GameObject TargetCrate;
    public Transform StartPosition;
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) {
	    if (col.gameObject == TargetCrate)
        { TargetCrate.transform.position = StartPosition.position; }
	}
}
