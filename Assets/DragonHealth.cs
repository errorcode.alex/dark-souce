﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

namespace MoreMountains.Tools
{
    public class DragonHealth : MonoBehaviour
    {

        public float HP,maxHP;

        public AudioClip HitSFX,DeadSFX ;
        public AudioSource _audio;

        public Animator _anim;
        public string LevelName;
        public string PlayerID = "Dragon";

        public void Start()
        {
            _anim = _anim.GetComponent<Animator>();
            
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Projectile")
            {
                _audio.PlayOneShot(HitSFX);
                GetComponent<MMHealthBar>().UpdateBar(HP,0,maxHP,true);

                if (HP == 0)
                {
                    _anim.SetTrigger("Fail");
                    _audio.PlayOneShot(DeadSFX);
                    //HP = 0;
                }
            }
        }

        public void Final()
        {
            MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.LevelComplete));
            MMEventManager.TriggerEvent(new MMGameEvent("Save"));
            LevelManager.Instance.GotoLevel("Level 20");
        }
    }
}

