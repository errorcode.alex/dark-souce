﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetLanguage : MonoBehaviour {


    public string curLang;

    public void SelectLanguage()
    {
        PlayerPrefs.SetString("UserLanguage",curLang); // Установить русский язык
        PlayerPrefs.Save(); // Сохранить
        
        SimpeTranslate.UpdateLanguage();

        //SceneManager.LoadScene("SplashDS",LoadSceneMode.Single);
    }
}
