﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChanteTextTimer : MonoBehaviour {

    public float MaxA,curA;
    public int counter;

    public Text changes;

    public string rus, eng, ger, chi, esp, fr;

	// Use this for initialization
	void Start () {

        changes = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        curA -= 0.1f;

        if (curA <= 0f)
        {
            curA = 0f;
            ChangeText();
        }
	}

    void ChangeText()
    {
        curA = MaxA;

        switch (counter)
        {
            case 0:
                changes.text = rus;
                break;

            case 1:
                changes.text = eng;
                break;

            case 2:
                changes.text = ger;
                break;

            case 3:
                changes.text = chi;
                break;

            case 4:
                changes.text = esp;
                break;

            case 5:
                changes.text = fr;
                break;

        }

        if (counter != 5)
        { counter++; }
        else
        { counter = 0; }
    }
}
