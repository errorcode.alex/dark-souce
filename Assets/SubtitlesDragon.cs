﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtitlesDragon : MonoBehaviour {

    [Multiline]
    public string[] Sub;
    [Multiline]
    public string[] subEng, subGer, subChinese, subSpanish, subFrench;

    public Text _text;
    public int _currentIndex;
	// Use this for initialization

	void Start () {

        // Определение языка
        if (PlayerPrefs.HasKey("UserLanguage"))
        {
            // На английском
            if (PlayerPrefs.GetString("UserLanguage") == "eng")
            { _text.text = subEng[_currentIndex]; }
            else
            // На немецком
            if (PlayerPrefs.GetString("UserLanguage") == "ger")
            { _text.text = subGer[_currentIndex]; }
            else
            // На китайском
            if (PlayerPrefs.GetString("UserLanguage") == "chi")
            { _text.text = subChinese[_currentIndex]; }
            else
            // На испанском
            if (PlayerPrefs.GetString("UserLanguage") == "esp")
            { _text.text = subSpanish[_currentIndex]; }
            else
            // На французском
            if (PlayerPrefs.GetString("UserLanguage") == "fr")
            { _text.text = subFrench[_currentIndex]; }
            else
            // На русском
            if (PlayerPrefs.GetString("UserLanguage") == "rus")
            { _text.text = Sub[_currentIndex]; }
        }
        else
        {
            _text.text = Sub[_currentIndex]; // По умолчанию русский язык
        }

       
    }

    public void UpdateSubtitleText()
    {
        if (_currentIndex != 5)
        { _currentIndex++; }


            // На английском
            if (PlayerPrefs.GetString("UserLanguage") == "eng")
            { _text.text = subEng[_currentIndex]; }
            else
            // На немецком
            if (PlayerPrefs.GetString("UserLanguage") == "ger")
            { _text.text = subGer[_currentIndex]; }
            else
            // На китайском
            if (PlayerPrefs.GetString("UserLanguage") == "chi")
            { _text.text = subChinese[_currentIndex]; }
            else
            // На испанском
            if (PlayerPrefs.GetString("UserLanguage") == "esp")
            { _text.text = subSpanish[_currentIndex]; }
            else
            // На французском
            if (PlayerPrefs.GetString("UserLanguage") == "fr")
            { _text.text = subFrench[_currentIndex]; }
            else
            // На русском
            if (PlayerPrefs.GetString("UserLanguage") == "rus")
            { _text.text = Sub[_currentIndex]; }
        

    }

    public void ClearSubs()
    {
        _text.text ="";
    }

}
