﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public class ShakeCamera : MonoBehaviour
    {
        public Vector3 ShakeParam;
        public bool PlayerNear;
        public CameraController _camera;
        // Use this for initialization
        void Start()
        {
            _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
        }

        // Update is called once per frame
        public void ShakeCam()
        {
            if (PlayerNear)
            { _camera.Shake(ShakeParam); }
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.tag == "Player")
            { PlayerNear = true; }
        }

        void OnTriggerExit2D(Collider2D col)
        {
            if (col.tag == "Player")
            { PlayerNear = false; }
        }


    }
}
