﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LewerScript : MonoBehaviour {

    public Animator _myAnim, _targetAnim;
    public string RunBoolTag;
    public bool RunOnOff = true;

	// Use this for initialization
	void Start () {
        _myAnim = _myAnim.GetComponent<Animator>();

        if (_targetAnim != null)
        { _targetAnim = _targetAnim.GetComponent<Animator>(); }

        _targetAnim.SetBool(RunBoolTag, RunOnOff);
    }

    public void InteractMechanism()
    {
        // Анимация рычага
        if (_myAnim.GetBool("Run") == false)
        { _myAnim.SetBool("Run", true); }
        else
        { _myAnim.SetBool("Run", false); }

        // Анимация моста/механизма
        if (_targetAnim.GetBool(RunBoolTag) == false)
        { _targetAnim.SetBool(RunBoolTag, true); }
        else
        { _targetAnim.SetBool(RunBoolTag, false); }
    }

}
