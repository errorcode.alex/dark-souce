﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LangManager : MonoBehaviour {

    public string CurLang; // Текущий язык rus eng ger chi

    private void Start()
    {
        if (PlayerPrefs.HasKey("UserLanguage"))
        {
            CurLang = PlayerPrefs.GetString("UserLanguage");
        }
        else
        {
            CurLang = "eng"; // По умолчанию английский язык
        }
    }
}
