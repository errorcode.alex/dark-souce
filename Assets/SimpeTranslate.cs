﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpeTranslate : MonoBehaviour
{


    public static List<SimpeTranslate> Instances = new List<SimpeTranslate>();
    public Text Line;
    public string lang;
    //public LangManager _lang;

        [Multiline]
    public string RussianText,EnglishText,GermanText,ChineseText,EspanolaText, FrenchText;

    private void OnEnable()
    {
        Instances.Add(this);
        SetLanguage();
    }
    
    private void OnDisable()
    {
        Instances.Remove(this);
    }

    // Use this for initialization
	void Start ()
    {
        SetLanguage();
    }

    private void SetLanguage()
    {
#if !UNITY_PS4
        if(!PlayerPrefs.IsLoad)
            return;
#endif
        // Определение языка
        if (PlayerPrefs.HasKey("UserLanguage"))
        {
            lang = PlayerPrefs.GetString("UserLanguage");
        }
        else
        {
            lang = "eng"; // По умолчанию английский язык
        }

        // Перевод фраз
        switch (lang)
        {
            case "rus":
                Line.text = RussianText; //"Разработчик Artalasky";
                break;

            case "eng":
                Line.text = EnglishText; // "Game by Artalasky";
                break;

            case "ger":
                Line.text = GermanText; // "Entwickler Artalasky";
                break;

            case "chi":
                Line.text = ChineseText; // "开发商 Artalasky";
                break;

            case "esp":
                Line.text = EspanolaText; // "Desarrollador Artalasky";
                break;

            case "fr":
                Line.text = FrenchText; // "Créateur d'Artalasky";
                break;
        }
    }

    public static void UpdateLanguage()
    {
        foreach (var instance in Instances)
            instance.SetLanguage();
    }
}
