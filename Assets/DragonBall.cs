﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.Tools
{
    public class DragonBall : MonoBehaviour
    {

        public GameObject ExplodeVFX; // 
        public GameObject DragonBoss;

        public Vector3 Force;

        // Use this for initialization
        void Start()
        {
            DragonBoss = GameObject.FindGameObjectWithTag("Dragon");
            gameObject.GetComponent<Rigidbody>().AddForce(Force, ForceMode.Impulse);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Dragon")
            {
                DragonBoss.GetComponent<DragonHealth>().HP -= 1;
                Instantiate(ExplodeVFX, gameObject.transform.position, gameObject.transform.rotation);
            }
        }
    }
}
