﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;

public class PS4Editor : Editor
{
    private static string masterVersionFullEU = "01.00";
    private static string masterVersionFullUS = "01.00";
    private static string productName = "Dark Sauce";
    private static string contentIDEU = "EP8009-CUSA25197_00-DARKSAUCE00000EU";
    private static string contentIDUS = "UP5817-CUSA25198_00-DARKSAUCE00000US";
    
    public static void TrophyEditor(string region)
    {
        var arguments = Path.Combine(Application.dataPath,
            string.Format("Porting/Platform/PS4/Editor/res/{0}/trophy.trp", region));
        var proc = new Process
        {
            StartInfo =
            {
                FileName = "orbis-pub-trp.exe",
                Arguments = arguments,
                WindowStyle = ProcessWindowStyle.Normal
            }
        };
        proc.Start();
    }
    

    public static void ParamSfo(string region)
    {
        var arguments = Path.Combine(Application.dataPath,
            string.Format("Porting/Platform/PS4/Editor/res/{0}/paramSFO.sfo", region));
        var proc = new Process
        {
            StartInfo =
            {
                FileName = "orbis-pub-sfo.exe",
                Arguments = arguments,
                WindowStyle = ProcessWindowStyle.Normal
            }
        };
        proc.Start();
    }
    
    [MenuItem("Tools/PS4/param.sfo EU")]
    public static void ParamSfoEU()
    {
        ParamSfo("EU");
    }
    
    [MenuItem("Tools/PS4/param.sfo US")]
    public static void ParamSfoUS()
    {
        ParamSfo("US");
    }
    
    [MenuItem("Tools/PS4/TrophyEditorEU")]
    public static void TrophyEditorEU()
    {
        TrophyEditor("EU");
    }
    
    [MenuItem("Tools/PS4/TrophyEditorUS")]
    public static void TrophyEditorUS()
    {
        TrophyEditor("US");
    }
    
    [MenuItem("Tools/PS4/Voice Recognition Title Name")]
    public static void VoiceRecognition()
    {
        var SCE_ROOT_DIR = Environment.GetEnvironmentVariable("SCE_ROOT_DIR");
        var path = Path.Combine(SCE_ROOT_DIR,
            "ORBIS/Tools/Voice Recognition Title Name Tool/bin/Voice_Recognition_Title_Name_Tool.exe");

        var proc = new Process
        {
            StartInfo =
            {
                FileName = path,
                UseShellExecute = true,
                Verb = "runas"
            }
        };
        proc.Start();
    }
    
    [MenuItem("Tools/PS4/Set Full version EU")]
    public static void SetFullEU()
    {
        UnityEditor.PlayerSettings.productName = productName;
        
        UnityEditor.PlayerSettings.PS4.masterVersion = masterVersionFullEU;
        UnityEditor.PlayerSettings.PS4.contentID = contentIDEU;
        UnityEditor.PlayerSettings.PS4.NPtitleDatPath = "Assets/Porting/Platform/PS4/Editor/res/EU/CUSA25197_00/nptitle.dat";
        UnityEditor.PlayerSettings.PS4.PronunciationXMLPath = "Assets/Porting/Platform/PS4/Editor/res/EU/Voice/pronunciation.xml";
        UnityEditor.PlayerSettings.PS4.PronunciationSIGPath = "Assets/Porting/Platform/PS4/Editor/res/EU/Voice/pronunciation.sig";

        UnityEditor.PlayerSettings.PS4.npTrophyPackPath = "Assets/Porting/Platform/PS4/Editor/res/EU/trophy.trp";
        
        UnityEditor.PlayerSettings.PS4.parentalLevel = 3;
        UnityEditor.PlayerSettings.PS4.npAgeRating = 7;

//        UnityEditor.PlayerSettings.PS4.paramSfxPath = "Assets/Porting/Platform/PS4/Editor/res/EU/paramSFX.sfx";
        
        var icon512X512 = EditorGUIUtility.Load("Assets/Porting/Platform/PS4/Editor/res/Icons/StillImageIcon_512x512.png") as Texture2D;
        Texture2D[] icons = {icon512X512};
        PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.PS4, icons);
        
        UnityEditor.PlayerSettings.PS4.SaveDataImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/SaveDataIcon_228x128.png";
        UnityEditor.PlayerSettings.PS4.BackgroundImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/BackgroundImage_1920x1080.png";
        
        UnityEditor.PlayerSettings.PS4.StartupImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/StartupImage_1920x1080.png";
        //UnityEditor.PlayerSettings.PS4.ShareOverlayImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/ShareOverlayImage_256x128.png";

        UnityEditor.PlayerSettings.PS4.appType = 0;
        
        Utility.RemoveDefineSymbol(BuildTargetGroup.PS4, "UNITY_US");
        Utility.AddDefineSymbol(BuildTargetGroup.PS4, "UNITY_EU");
        Utility.RemoveDefineSymbol(BuildTargetGroup.PS4, "SPANISH_LATIN");
    }
    
    [MenuItem("Tools/PS4/Set Full version US")]
    public static void SetFullUS()
    {
        UnityEditor.PlayerSettings.productName = productName;

        UnityEditor.PlayerSettings.PS4.masterVersion = masterVersionFullUS;
        UnityEditor.PlayerSettings.PS4.contentID = contentIDUS;
        UnityEditor.PlayerSettings.PS4.NPtitleDatPath = "Assets/Porting/Platform/PS4/Editor/res/US/CUSA25198_00/nptitle.dat";
        
        UnityEditor.PlayerSettings.PS4.PronunciationXMLPath = "Assets/Porting/Platform/PS4/Editor/res/US/Voice/pronunciation.xml";
        UnityEditor.PlayerSettings.PS4.PronunciationSIGPath = "Assets/Porting/Platform/PS4/Editor/res/US/Voice/pronunciation.sig";

        UnityEditor.PlayerSettings.PS4.npTrophyPackPath = "Assets/Porting/Platform/PS4/Editor/res/US/trophy.trp";
        
        UnityEditor.PlayerSettings.PS4.parentalLevel = 4;
        UnityEditor.PlayerSettings.PS4.npAgeRating = 10;

//        UnityEditor.PlayerSettings.PS4.paramSfxPath = "Assets/Porting/Platform/PS4/Editor/res/US/paramSFX.sfx";

        var icon512X512 = EditorGUIUtility.Load("Assets/Porting/Platform/PS4/Editor/res/Icons/StillImageIcon_512x512.png") as Texture2D;
        Texture2D[] icons = {icon512X512};
        PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.PS4, icons);
        
        UnityEditor.PlayerSettings.PS4.SaveDataImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/SaveDataIcon_228x128.png";
        UnityEditor.PlayerSettings.PS4.BackgroundImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/BackgroundImage_1920x1080.png";
        
        UnityEditor.PlayerSettings.PS4.StartupImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/StartupImage_1920x1080.png";
        //UnityEditor.PlayerSettings.PS4.ShareOverlayImagePath = "Assets/Porting/Platform/PS4/Editor/res/Icons/ShareOverlayImage_256x128.png";

        UnityEditor.PlayerSettings.PS4.appType = 0;
        
        Utility.RemoveDefineSymbol(BuildTargetGroup.PS4, "UNITY_EU");
        Utility.AddDefineSymbol(BuildTargetGroup.PS4, "UNITY_US");
        Utility.AddDefineSymbol(BuildTargetGroup.PS4, "SPANISH_LATIN");
    }
}