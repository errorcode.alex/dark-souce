﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum ButtonIcon
{
    button_action_top,
    button_action_bottom,
    button_action_right,
    button_action_left,
    button_shoulder_left,
    button_shoulder_right,
    button_d_pad,
    button_start,
    stick_left,
    stick_right,
    button_action_confirm,
    button_action_cancel,
    button_trigger_left,
    button_trigger_right,
}

public class ButtonPlatformDifference : MonoBehaviour
{
    [SerializeField]
    private ButtonIcon button;
    [SerializeField]
    private GameObject buttonGO;
    void Start()
    {
        if (buttonGO != null)
        {
            Destroy(buttonGO);
            buttonGO = null;
        }

        var pathToButtons = "buttons/";
        
#if UNITY_SWITCH_UI
        pathToButtons = Path.Combine(pathToButtons, "Switch/");
#elif UNITY_PS4_UI
        pathToButtons = Path.Combine(pathToButtons, "PS4/");
#elif UNITY_XBOXONE_UI
        pathToButtons = Path.Combine(pathToButtons, "XboxOne/");
#endif
        
        switch (button)
        {
            case ButtonIcon.button_action_top:
                pathToButtons = Path.Combine(pathToButtons, "TopButton");
                break;
            case ButtonIcon.button_action_bottom:
                pathToButtons = Path.Combine(pathToButtons, "TopBottom");
                break;
            case ButtonIcon.button_action_right:
                pathToButtons = Path.Combine(pathToButtons, "LeftRight");
                break;
            case ButtonIcon.button_action_left:
                pathToButtons = Path.Combine(pathToButtons, "LeftButton");
                break;
            case ButtonIcon.button_shoulder_left:
                pathToButtons = Path.Combine(pathToButtons, "ShoulderL");
                break;
            case ButtonIcon.button_shoulder_right:
                pathToButtons = Path.Combine(pathToButtons, "ShoulderR");
                break;
            case ButtonIcon.button_d_pad:
                pathToButtons = Path.Combine(pathToButtons, "DPad");
                break;
            case ButtonIcon.button_start:
                pathToButtons = Path.Combine(pathToButtons, "SettingsButton");
                break;
            case ButtonIcon.stick_left:
                pathToButtons = Path.Combine(pathToButtons, "LeftStick");
                break;
            case ButtonIcon.stick_right:
                pathToButtons = Path.Combine(pathToButtons, "RightStick");
                break;
            case ButtonIcon.button_action_confirm:
                pathToButtons = Path.Combine(pathToButtons, "ConfirmButton");
                break;
            case ButtonIcon.button_action_cancel:
                pathToButtons = Path.Combine(pathToButtons, "CancelButton");
                break;
            case ButtonIcon.button_trigger_left:
                pathToButtons = Path.Combine(pathToButtons, "TriggerL");
                break;
            case ButtonIcon.button_trigger_right:
                pathToButtons = Path.Combine(pathToButtons, "TriggerR");
                break;
        }

//        Debug.LogError(pathToButtons);
        var buttonPref = Resources.Load(pathToButtons);

        buttonGO = Instantiate(buttonPref, transform) as GameObject;
    }
}
