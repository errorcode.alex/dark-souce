﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screen1
{

    /*
    public static float aspectRatioMax
    {
        get
        {
            return 20f / 9;
        }
    }
    */
    /*
    public static int width
    {
        
        get
        {
            var width = UnityEngine.Screen.width;
            width = (int)(width * input_setup_s.instance.scaleFactor);
            return width;
        }
        
    }

    public static int height
    {
        get
        {
            var height = UnityEngine.Screen.height;
            height = (int)(height * input_setup_s.instance.scaleFactor);
            return height;
        }
    }
*/
    public static bool fullScreen
    {
        get { return UnityEngine.Screen.fullScreen; }
        set { UnityEngine.Screen.fullScreen = value; }
    }
    
    public static Resolution[] resolutions
    {
        get { return UnityEngine.Screen.resolutions; }
    }
    
    public static Resolution currentResolution
    {
        get { return UnityEngine.Screen.currentResolution; }
    }

    public static int sleepTimeout
    {
        get { return UnityEngine.Screen.sleepTimeout; }
        set { UnityEngine.Screen.sleepTimeout = value; }
    }
    
    public static void SetResolution(int width, int height, bool fullscreen)
    {
        UnityEngine.Screen.SetResolution(width, height, fullscreen, 0);
    }
    
}