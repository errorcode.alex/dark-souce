﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventToggle : MonoBehaviour
{
    public UnityEvent onShow;
    public UnityEvent onHide;

    private void OnEnable()
    {
        onShow.Invoke();
    }

    private void OnDisable()
    {
        onHide.Invoke();
    }
}
