﻿using Rewired;
using UnityEngine;
using UnityEngine.Events;

public class InputListner : MonoBehaviour
{
    [SerializeField]
    private string buttonName;
    private Player _player;

    public UnityEvent onEvent;
    
    private void Start()
    {
        _player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        if (_player.GetButtonDown(buttonName))
            onEvent.Invoke();
    }
    
}
