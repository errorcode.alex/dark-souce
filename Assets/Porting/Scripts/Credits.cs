﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    public Transform text;
    public float speed;
    public InputListner InputListner;

    public float exitY;
    public float initY;
    
    private void OnEnable()
    {
        var vecTmp = text.position;
        vecTmp.y = initY;
        text.position = vecTmp;
    }

    void Update()
    {
        if(text.position.y > exitY)
            InputListner.onEvent.Invoke();
        
        var vecTmp = text.position;
        vecTmp.y += speed * Time.deltaTime;
        text.position = vecTmp;
    }
}
