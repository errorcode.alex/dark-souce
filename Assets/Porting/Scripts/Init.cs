﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Init : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(LoadScene("MainMenu"));
    }

    IEnumerator LoadAsync(string sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            Debug.Log(operation.progress);

            yield return null;
        }
    }

    IEnumerator LoadScene(string sceneIndex)
    {
#if UNITY_XBOXONE && !UNITY_EDITOR
        yield return new WaitForSeconds(8);
#endif
#if UNITY_PS4 && !UNITY_EDITOR
        yield return new WaitForSeconds(8);
#endif
#if UNITY_SWITCH && !UNITY_EDITOR
        yield return new WaitForSeconds(8);
#endif
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneIndex);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
      //  Debug.Log("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            //Debug.LogError("Loading progress: " + (asyncOperation.progress * 100) + "%");

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Change the Text to show the Scene is ready
                //Debug.LogError("Press the space bar to continue");
                
                //Wait to you press the space key to activate the Scene
                //if (Input.GetKeyDown(KeyCode.Space))
                    //Activate the Scene
                    asyncOperation.allowSceneActivation = true;
                    yield break;
            }

            yield return null;
        }
    }
}
