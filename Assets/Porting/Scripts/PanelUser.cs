﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelUser : MonoBehaviour
{
    public int depth;
    public float x;
    public float y;
    public float width;
    public float height;
    public virtual void OnGUI()
    {
        GUI.depth = this.depth;
        GUI.Box(new Rect(x, y, width, height), "");

    }
}
