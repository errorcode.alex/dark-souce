﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Utility : Editor
{
    [MenuItem("Tools/Screenshot")]
    public static void Screenshot()
    {
        var path = Path.Combine(Application.streamingAssetsPath, "screen.png");
        ScreenCapture.CaptureScreenshot(path);
    }
    public static void AddDefineSymbol(BuildTargetGroup targetGroup, string defineName)
    {
        var defines = UnityEditor.PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup);
        char[] separator = {';'}; 
        var definesList = defines.Split(separator, StringSplitOptions.RemoveEmptyEntries);

        if (definesList.Any(t => t == defineName))
            return;
        
        var newDefines = definesList.Aggregate("", (current, s) => current + (s + ";"));

        newDefines += defineName;
        UnityEditor.PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, newDefines);
    }
    
    public static void RemoveDefineSymbol(BuildTargetGroup targetGroup, string defineName)
    {
        var defines = UnityEditor.PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup);
        char[] separator = {';'}; 
        var definesList = defines.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        
        var newDefines = definesList.Where(t => t != defineName).Aggregate("", (current, t) => current + (t + ";"));
        
        UnityEditor.PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, newDefines);
    }
}
