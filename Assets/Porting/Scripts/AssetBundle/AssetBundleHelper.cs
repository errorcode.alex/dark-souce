﻿using UnityEngine;

public class AssetBundleHelper
{
    private AssetBundle currentAssetBundle;
    public T LoadAsset<T>(string assetBundleName, string objectNameToLoad) where T : Object
    {
        var filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "AssetBundles");
        filePath = System.IO.Path.Combine(filePath, assetBundleName);
        
        currentAssetBundle = AssetBundle.LoadFromFile(filePath);
       
        var asset = currentAssetBundle.LoadAsset<T>(objectNameToLoad);
        return asset;
    }

    public void UnloadAsset()
    {
        if (currentAssetBundle != null)
            currentAssetBundle.Unload(true);
    }
}
