
using System;
using System.Collections;

public interface IStorage
{
    void Init();
    void Update();

    IEnumerator SaveAsync(string fileName, byte[] content, Action onComplete = null);

    IEnumerator SaveAsync(string fileName, string content, Action onComplete = null);

    IEnumerator ReadAsync(string fileName, Action<byte[]> onComplete);

    IEnumerator ReadTextAsync(string fileName, Action<string> onComplete);

    IEnumerator FileExistsAsync(string fileName, Action<bool> onExists);

    IEnumerator DeleteFileAsync(string fileName, Action onComplete = null);
    
    
    void Save(string fileName, byte[] content);

    void Save(string fileName, string content);

    byte[] Read(string fileName);

    string ReadText(string fileName);

    bool FileExists(string fileName);

    void DeleteFile(string fileName);

    void Cleanup();
}
