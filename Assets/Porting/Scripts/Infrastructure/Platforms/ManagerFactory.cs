﻿using UnityEngine;

public static class ManagerFactory
{
    public static BaseManager Create(GameObject gameObject)
    {
#if UNITY_SWITCH
        return gameObject.AddComponent<SwitchManager>();
#elif UNITY_PS4
        return gameObject.AddComponent<PS4Manager>();
#elif UNITY_XBOXONE
        return gameObject.AddComponent<XboxOneManager>();
#else
        return gameObject.AddComponent<BaseManager>();
        
#endif
    }
}