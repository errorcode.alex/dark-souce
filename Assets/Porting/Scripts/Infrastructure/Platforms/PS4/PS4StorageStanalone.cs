﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
 
public class PS4StorageStanalone : IStorage
{
    private string subTitle = "Save Data";
    private string mgiSaveData = "DC_SAVE_DATA";

    private string FileNameToKey(string saveId)
    {
        var key = string.Format("{0}{1}", mgiSaveData, saveId);
        Debug.LogError("key : " + key);
        return key;
    }
    
    public void Init()
    {
        // Limit PS4PlayerPrefs 8MB
        // but in PS4 SDK 5.5 and later change limit PS4PlayerPrefs on ~32MB
        
        OnScreenLog.Instance.Add("PS4Storage. Init. Started");
        

        const string ignoreData = "VARIABLE_IGNORE";
        PlayerPrefs.SetInt(ignoreData, 1);

        var title = Application.productName;
        var detail = string.Format("Save game data for {0}", title);
        
    }

    public void Update()
    {
    }

    public IEnumerator SaveAsync(string fileName, byte[] content, Action onComplete = null)
    {
        Save(fileName, content);
        if(onComplete != null)
            onComplete.Invoke();
        yield return null;
    }

    public IEnumerator SaveAsync(string fileName, string content, Action onComplete = null)
    {
        var bytes = Convert.FromBase64String(content);
        yield return SaveAsync(fileName, bytes, onComplete);
    }

    public void Save(string fileName, byte[] content)
    {
        OnScreenLog.Instance.Add(string.Format("Save byte fileName : {0}, count byte : {1}", fileName, content.Length));
        var stringData = Convert.ToBase64String(content);
        PlayerPrefs.SetString(FileNameToKey(fileName), stringData);
        PlayerPrefs.Save();
    }

    public void Save(string fileName, string content)
    {
        OnScreenLog.Instance.Add(string.Format("Save string fileName : {0}, content : {1}", fileName, content));
        var bytes = Encoding.UTF8.GetBytes(content);
        Save(fileName, bytes);
    }

    public IEnumerator ReadAsync(string fileName, Action<byte[]> onComplete)
    {
        if(onComplete != null)
            onComplete.Invoke(Read(fileName));
        
        yield return null;
    }

    public IEnumerator ReadTextAsync(string fileName, Action<string> onComplete)
    {
        if(onComplete != null)
            onComplete.Invoke(ReadText(fileName));
        
        yield return null;
    }

    public byte[] Read(string fileName)
    {
        var stringData = ReadText(fileName);
        var bytesData = Convert.FromBase64String(stringData);
        OnScreenLog.Instance.Add(string.Format("Read byte fileName : {0}, count byte : {1}", fileName, bytesData.Length));
        return bytesData;
    }

    public string ReadText(string fileName)
    {
        OnScreenLog.Instance.Add(string.Format("ReadText fileName : {0}", fileName));
        return PlayerPrefs.GetString(FileNameToKey(fileName));
    }

    public IEnumerator FileExistsAsync(string fileName, Action<bool> onExists)
    {
        if(onExists != null)
            onExists.Invoke(FileExists(fileName));
        
        yield return null;
    }

    public bool FileExists(string fileName)
    {
        return PlayerPrefs.HasKey(FileNameToKey(fileName));
    }
    
    public IEnumerator DeleteFileAsync(string fileName, Action onComplete = null)
    {
        DeleteFile(fileName);
        if(onComplete != null)
            onComplete.Invoke();
        yield return null;
    }

    public void DeleteFile(string fileName)
    {
        PlayerPrefs.DeleteKey(FileNameToKey(fileName));
    }

    public IEnumerator DeleteFile(string fileName, bool useDialogs)
    {
        DeleteFile(fileName);
        yield return null;
    }
        
    public void Cleanup()
    {
        PlayerPrefs.Save();
    }
}