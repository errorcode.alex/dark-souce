﻿#if UNITY_SWITCH || UNITY_XBOXONE || UNITY_STANDALONE || UNITY_EDITOR && (UNITY_SWITCH || UNITY_XBOXONE)
using Consoles;
using System.Collections.Generic;
using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public static class PlayerPrefs
{
    private static Dictionary<string, string> _keys;
    
    private static Dictionary<string, string> _keysSave;
    private const string FileName = "PlayerPrefs.json";
    
    
    public static bool IsLoad
    {
        get { return _keys != null; }
    }

    private static void Set(string key, string value)
    {
        Load();
        if (_keys == null)
            return;
        _keys[key] = value;
        //Save();
    }

    private static string Get(string key)
    {
        Load();
        return _keys[key];
    }
    
    private static bool Is(string key)
    {
        Load();
        if (_keys == null)
            return false;
        return _keys.ContainsKey(key);
    }

    private static void Remove(string key = null)
    {
        Load();
        if(key == null)
            _keys.Clear();
        else
            _keys.Remove(key);
        Save();
    }

    public static void SetInt(string key, int value)
    {
        Set(key, value.ToString());
    }

    public static int GetInt(string key, int defaultValue = 0)
    {
        return Is(key) ? Convert.ToInt32(Get(key)) : defaultValue;
    }

    public static void SetFloat(string key, float value)
    {
        Set(key, value.ToString(CultureInfo.InvariantCulture));
    }

    public static float GetFloat(string key, float defaultValue = 0)
    {
        return Is(key) ? float.Parse(Get(key), CultureInfo.InvariantCulture) : defaultValue;
    }

    public static void SetString(string key, string value)
    {
//        Debug.LogError("SetString : " + key);
        Set(key, value);
    }

    public static string GetString(string key, string defaultValue = "")
    {
        return Is(key) ? Get(key) : defaultValue;
    }

    public static bool HasKey(string key)
    {
        return Is(key);
    }

    public static void DeleteKey(string key)
    {
        Remove(key);
    }

    public static void DeleteAll()
    {
//        Debug.LogError("DeleteAll");
        Remove();
    }
    
    public static void SaveStateChanges()
    {
        if(_keysSave == null || _keys == null)
            return;
        
        if (_keysSave != _keys)
            Save();
    }
    
    public static void Save(Action onComplete = null)
    {
#if UNITY_SWITCH
        _keysSave = new Dictionary<string, string>(_keys);
        Infrastructure.Instance.Storage.Save(FileName, ToJson(_keysSave));
        if(onComplete != null)
            onComplete.Invoke();
#else
        
        _keysSave = new Dictionary<string, string>(_keys);

        Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.SaveAsync(FileName, ToJson(_keysSave), onComplete));
#endif
//        Debug.LogError("Save to file");
    }

    public static void ReLoad(Action onComplete = null)
    {
//        Debug.LogError("ReLoad");
        _keys = null;
        _keysSave = null;
        Load(onComplete);
    }
    private static void Load(Action onComplete = null)
    {
        //if (!Infrastructure.Instance.XboxSignInUser.authorized)
        //    return;

        if (_keys != null)
            return;
//        Debug.LogError("Load");
        _keys = new Dictionary<string, string>();
        
#if UNITY_SWITCH
        if (!Infrastructure.Instance.Storage.FileExists(FileName))
        {
            if(onComplete != null)
                onComplete.Invoke();
            return;
        }

        var data = Infrastructure.Instance.Storage.ReadText(FileName);
        _keys = ParseJson<Dictionary<string, string>>(data);
        _keysSave = new Dictionary<string, string>(_keys);
        if(onComplete != null)
            onComplete.Invoke();
#else
        Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.FileExistsAsync(FileName, exists =>
        {
            if (exists)
            {
                Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.ReadTextAsync(FileName, data =>
                {
                    _keys = ParseJson<Dictionary<string, string>>(data);
                    _keysSave = new Dictionary<string, string>(_keys);
                    Debug.LogError("_keysSave Load");
                    
                    foreach (var _keySavs in _keysSave)
                    {
                        Debug.LogError("_keySavs : " + _keySavs);
                    }
                    if(onComplete != null)
                        onComplete.Invoke();
                }));
            }
            else
            {
                if(onComplete != null)
                    onComplete.Invoke();
            }
        }));
#endif
    }
    
    public static T ParseJson<T>(String jsonText)
    {
        var jsonSerializer = new JsonSerializer();

        using (var stringReader = new StringReader(jsonText))
        {
            using (var jsonReader = new JsonTextReader(stringReader))
            {
                try
                {
                    return jsonSerializer.Deserialize<T>(jsonReader);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    throw;
                }
            }
        }
    }

    public static string ToJson<T>(T obj)
    {
        var jsonSerializer = new JsonSerializer();

        using (var textWriter = new StringWriter())
        {
            using (var jsonWriter = new JsonTextWriter(textWriter))
            {
                try
                {
                    jsonSerializer.Serialize(jsonWriter, obj);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    throw;
                }
            }

            return textWriter.ToString();
        }
    }
}
#endif