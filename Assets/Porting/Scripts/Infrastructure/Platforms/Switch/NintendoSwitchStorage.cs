using System;
using System.Collections;
using System.IO;
using System.Text;
using Consoles;
using UnityEngine;


#if UNITY_SWITCH && !UNITY_EDITOR
public class NintendoSwitchStorage : IStorage
{
    //private nn.account.Uid userId;
    private const string MountName = "MySave"; //saveData
    private const string saveDataPath = MountName + ":/";
    private string applicationPath;

    private const int SaveDataJurnalSize = 10485760; // SaveDataSize = 10485760;
    private const int Kb16 = 16348;
    private const int JournalSaveDataSize = SaveDataJurnalSize - Kb16 * 2;
    private bool wasInitialized = false;
    
    private int _countSaves;
    private double _lastInterval;
    private float _savesPerMinutes;
    private const float LimitsFrequencyWriting = 32f;
    private const float UpdateInterval = 60f;
    
    ~NintendoSwitchStorage()
    {
        Cleanup();
    }

    private string GetFullPath(string fileName)
    {
        if (string.IsNullOrEmpty(applicationPath))
            throw new NullReferenceException("applicationPath must not be empty");

        return applicationPath + fileName;
    }

    private static byte[] ReadBytes(byte[] data)
    {
        byte[] dataByte;
        using (var stream = new MemoryStream(data))
        {
            var reader = new BinaryReader(stream);
            var sizeData = reader.ReadInt32();
            dataByte = reader.ReadBytes(sizeData);
        }

        return dataByte;
    }

    private static byte[] WriteBytes(byte[] content)
    {
        var lengthFile = content.Length + sizeof(int);
        var countBlocks = Mathf.CeilToInt(lengthFile / Kb16) + 1;
        var lengthBlocks = countBlocks * Kb16;
        
        byte[] data;
        using (var stream = new MemoryStream(lengthBlocks))
        {
            var writer = new BinaryWriter(stream);
            writer.Write(content.Length);
            writer.Write(content);
            stream.Close();
            data = stream.GetBuffer();
        }

        return data;
    }

    private static void ThrowException(nn.Result result, object param)
    {
        if (result.IsSuccess())
            return;

        if (nn.fs.FileSystem.ResultDirectoryNotEmpty.Includes(result))
            throw new Exception(string.Format("ResultDirectoryNotEmpty {0}", param));
        if (nn.fs.FileSystem.ResultDirectoryStatusChanged.Includes(result))
            throw new Exception(string.Format("ResultDirectoryStatusChanged {0}", param));
        if (nn.fs.FileSystem.ResultMountNameAlreadyExists.Includes(result))
            throw new Exception(string.Format("ResultMountNameAlreadyExists {0}", param));
        if (nn.fs.FileSystem.ResultPathAlreadyExists.Includes(result))
            throw new Exception(string.Format("ResultPathAlreadyExists {0}", param));
        if (nn.fs.FileSystem.ResultPathNotFound.Includes(result))
            throw new Exception(string.Format("ResultPathNotFound {0}", param));
        if (nn.fs.FileSystem.ResultTargetLocked.Includes(result))
            throw new Exception(string.Format("ResultTargetLocked {0}", param));
        if (nn.fs.FileSystem.ResultTargetNotFound.Includes(result))
            throw new Exception(string.Format("ResultTargetNotFound {0}", param));
        if (nn.fs.FileSystem.ResultUnsupportedSdkVersion.Includes(result))
            throw new Exception(string.Format("ResultUnsupportedSdkVersion {0}", param));
        if (nn.fs.FileSystem.ResultUsableSpaceNotEnough.Includes(result))
            throw new Exception(string.Format("ResultUsableSpaceNotEnough {0}", param));
    }

    #region implementation_IStorage

    public void Init()
    {
        _lastInterval = Time.realtimeSinceStartup;
        
        if (wasInitialized)
            return;
/*
        nn.account.Account.Initialize();
        var userHandle = new nn.account.UserHandle();
        nn.account.Account.TryOpenPreselectedUser(ref userHandle);
        nn.account.Account.GetUserId(ref userId, userHandle);
*/
        var switchManager = Infrastructure.Instance.Manager as SwitchManager;
        var result = nn.fs.SaveData.Mount(MountName, switchManager.GetUserId());
        result.abortUnlessSuccess();
        ThrowException(result, MountName);

        applicationPath = saveDataPath;
        wasInitialized = true;
    }

    public void Update()
    {
    }

    public IEnumerator SaveAsync(string fileName, byte[] content, Action onComplete = null)
    {
        Save(fileName, content);
        
        if(onComplete != null)
            onComplete.Invoke();
        yield return null;
    }

    public IEnumerator SaveAsync(string fileName, string content, Action onComplete = null)
    {
        yield return SaveAsync(fileName, Encoding.UTF8.GetBytes(content), onComplete);
    }

    public IEnumerator ReadAsync(string fileName, Action<byte[]> onComplete)
    {
        if(onComplete != null)
            onComplete.Invoke(Read(fileName));
        
        yield return null;
    }

    public IEnumerator ReadTextAsync(string fileName, Action<string> onComplete)
    {
        if(onComplete != null)
            onComplete.Invoke(ReadText(fileName));
        
        yield return null;
    }
    public IEnumerator FileExistsAsync(string fileName, Action<bool> onExists)
    {
        if(onExists != null)
            onExists.Invoke(FileExists(fileName));
        
        yield return null;
    }

    public IEnumerator DeleteFileAsync(string fileName, Action onComplete = null)
    {
        DeleteFile(fileName);
        yield return null;
    }

    public void Save(string fileName, byte[] content)
    {
        ++_countSaves;
        var timeNow = Time.realtimeSinceStartup;
        if (timeNow > _lastInterval + UpdateInterval)
        {
            _savesPerMinutes = _countSaves / ((timeNow - (float)_lastInterval) / 60);
            _countSaves = 0;
            _lastInterval = timeNow;
            
            Debug.LogWarningFormat("<color={0}>frequency of writing to storage media {1} saves per minutes </color>",
                _savesPerMinutes >= LimitsFrequencyWriting ? "red" : "green", _savesPerMinutes);
        }
        if (content.Length >= JournalSaveDataSize - sizeof(int))
            throw new Exception(string.Format("The file with the size ({0}) has exceeded the allowed size {1}",
                content.Length, JournalSaveDataSize - sizeof(int)));

        var data = WriteBytes(content);

#if UNITY_SWITCH && !UNITY_EDITOR
        UnityEngine.Switch.Notification.EnterExitRequestHandlingSection();
#endif
        
        if (FileExists(fileName))
            DeleteFile(fileName);

        var fullPath = GetFullPath(fileName);
        var fileHandle = new nn.fs.FileHandle();
        var result = nn.fs.File.Create(fullPath, data.LongLength);
        ThrowException(result, "Create " + fullPath);

        result = nn.fs.File.Open(ref fileHandle, fullPath, nn.fs.OpenFileMode.Write);
        ThrowException(result, "Open " + fullPath);

        result = nn.fs.File.Write(fileHandle, 0, data, data.LongLength, nn.fs.WriteOption.Flush);
        ThrowException(result, "Write " + fullPath);

        nn.fs.File.Close(fileHandle);

        result = nn.fs.FileSystem.Commit(MountName);
        ThrowException(result, MountName);
        
#if UNITY_SWITCH && !UNITY_EDITOR
        UnityEngine.Switch.Notification.LeaveExitRequestHandlingSection();
#endif
    }

    public void Save(string fileName, string content)
    {
        Save(fileName, Encoding.UTF8.GetBytes(content));
    }

    public byte[] Read(string fileName)
    {
        var fullPath = GetFullPath(fileName);

        var fileHandle = new nn.fs.FileHandle();

        var result = nn.fs.File.Open(ref fileHandle, fullPath, nn.fs.OpenFileMode.Read);
        ThrowException(result, fullPath);

        long fileSize = 0;
        result = nn.fs.File.GetSize(ref fileSize, fileHandle);
        result.abortUnlessSuccess();

        var data = new byte[fileSize];
        result = nn.fs.File.Read(fileHandle, 0, data, fileSize);
        ThrowException(result, fullPath);

        nn.fs.File.Close(fileHandle);

        var dataByte = ReadBytes(data);

        return dataByte;
    }

    public string ReadText(string fileName)
    {
        var content = Encoding.UTF8.GetString(Read(fileName));
        return content;
    }

    public bool FileExists(string fileName)
    {
        nn.fs.EntryType entryType = 0;
        var result = nn.fs.FileSystem.GetEntryType(ref entryType, GetFullPath(fileName));
        return !nn.fs.FileSystem.ResultPathNotFound.Includes(result);
    }

    public void DeleteFile(string fileName)
    {
        var result = nn.fs.File.Delete(GetFullPath(fileName));
        ThrowException(result, GetFullPath(fileName));

        result = nn.fs.FileSystem.Commit(MountName);
        ThrowException(result, MountName);
    }

    public void DeleteFile(string profilePath, bool useDialogs)
    {
        DeleteFile(profilePath);
    }

    public void Cleanup()
    {
        if (wasInitialized)
            nn.fs.FileSystem.Unmount(MountName);

        wasInitialized = false;
    }

    #endregion
}
#endif
