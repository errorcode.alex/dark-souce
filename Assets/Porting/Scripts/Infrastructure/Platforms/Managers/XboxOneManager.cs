﻿
using System.Linq;
#if UNITY_XBOXONE
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Consoles;
using DataPlatform;
using Rewired;
using Sony.NP;
using Storage;
using UnityEngine;
using Users;


public static class XBoxOneStorageUtils
{
    public static int UserId = -1;
    public static int GamepadId = -1;
    public static string GameDisplayName = string.Empty;
}
public class XboxOneManager : BaseManager
{
    
    public static XboxOneManager Instance;

    public override void Init()
    {
        Instance = this;
        
#if !UNITY_EDITOR
        DataPlatformPlugin.InitializePlugin(0);

        // This really does nothing other than start up the plugin
        // and start the process of plugins discovering each other.
        StorageManager.Create();

        // The most efficient way to have plugins find each other
        // is to initialize them from leaf to root of the dependency
        // tree. In this case I am initializing the users manager
        // after the storage systems as storage requires users.
        // This is not required but ensures maximum efficiency.
        UsersManager.Create();
        
        // This method will ensure that the AchievementsManager exists.  The object should be 
        // allocated to live across scene boundaries.
        AchievementsManager.Create();
#endif
        // If this is a new account with 0 achievements to the name this will trigger.
        AchievementsManager.OnUnlockNotifications += OnUnlockNotifications;

        UsersManager.OnUsersChanged += OnUserChanged;
        UsersManager.OnUserSignOut += OnUserSignOut;
        XboxOnePLM.OnSuspendingEvent += Suspending;
        XboxOnePLM.OnResumingEvent += OnResumingEvent;
        
        ReInput.ControllerDisconnectedEvent += OnControllerDisconnected;
        ReInput.ControllerConnectedEvent += OnControllerConnected;
        ReInput.ControllerPreDisconnectEvent += OnControllerPreDisconnect;
    }

    public override void Cleanup()
    {
        AchievementsManager.OnUnlockNotifications -= OnUnlockNotifications;

        UsersManager.OnUsersChanged -= OnUserChanged;
        UsersManager.OnUserSignOut -= OnUserSignOut;
        XboxOnePLM.OnSuspendingEvent -= Suspending;
        XboxOnePLM.OnResumingEvent -= OnResumingEvent;
        
        ReInput.ControllerDisconnectedEvent -= OnControllerDisconnected;
        ReInput.ControllerConnectedEvent -= OnControllerConnected;
        ReInput.ControllerPreDisconnectEvent -= OnControllerPreDisconnect;
    }

    private static void OnControllerDisconnected(ControllerStatusChangedEventArgs args)
    {
        if (ReInput.controllers.Controllers.Any(controller => controller.isConnected && controller.type == ControllerType.Joystick))
            return;
        
        if(Instance.onPause != null)
            Instance.onPause.Invoke();
    }

    private static void OnControllerConnected(ControllerStatusChangedEventArgs args)
    {
        if(Instance.onUnPause != null)
            Instance.onUnPause.Invoke();
    }

    private static void OnControllerPreDisconnect(ControllerStatusChangedEventArgs args)
    {
        Debug.LogError("OnControllerPreDisconnect");
        //Instance.messagebox = Instantiate(Instance.prefabMessagebox);
    }
#if !UNITY_EDITOR
    public override string GetUserName()
    {
        return XBoxOneStorageUtils.GameDisplayName;
    }
    public override string GetUserID()
    {
        return XBoxOneStorageUtils.UserId.ToString();
    }
#endif

    public void TestOnUserSignOut()
    {
        if(onUserSignOut != null)
            onUserSignOut.Invoke();
    }
    private void OnUserSignOut(int id)
    {
        Debug.LogError("OnUserSignOut id=" + id);
        Debug.LogError("XBoxOneStorageUtils.UserId id=" + XBoxOneStorageUtils.UserId);

        if (XBoxOneStorageUtils.UserId == id)
        {
            DisconnectStorage();
            if(onUserSignOut != null)
                onUserSignOut.Invoke();
            //TODO: Перезагрузить игру
        }
    }

    private void OnUserChanged(int id, bool wasadded)
    {
        Debug.LogError("OnUserSignOut id=" + id + " wasadded = "+wasadded);
    }

    private void Suspending()
    {
        OnScreenLog.Instance.Add("Storage is still active suspending in a bit.");

        if ((XBoxOneStorage)Infrastructure.Instance.Storage == null)
        {
            XboxOnePLM.AmReadyToSuspendNow();
            return;
        }
        StartCoroutine(((XBoxOneStorage)Infrastructure.Instance.Storage).OnSuspend(() =>
        {
            OnScreenLog.Instance.Add("Storage disposed, shutting down now.");
            XboxOnePLM.AmReadyToSuspendNow();
        }));
    }
    
    private void OnResumingEvent(double secondsSuspended)
    {
        ((XBoxOneStorage)Infrastructure.Instance.Storage).OnResume(XBoxOneStorageUtils.UserId, secondsSuspended);
    }

    private void UpdateAchievementCallback(UnityPlugin.AsyncStatus status, UnityAOT.ActionAsyncOp op)
    {
        OnScreenLog.Instance.Add("UpdateAchievementAsync call complete: " + op.IsComplete);
        OnScreenLog.Instance.Add("UpdateAchievementAsync success: " + op.Success);
        OnScreenLog.Instance.Add("UpdateAchievementAsync HRESULT:  0x" + op.Result.ToString("X8"));
        OnScreenLog.Instance.Add("UpdateAchievementAsync returned status: " + status.GetHashCode());
    }

    public override void UnlockingAchievement(string id)
    {
        Debug.LogError(id);
        var platformId = id;
        var trophyIndex = int.Parse(platformId, CultureInfo.InvariantCulture);
        OnScreenLog.Instance.Add("UnlockTrophy. Trophy Id = " + trophyIndex);
        
        User currentUser = UsersManager.FindUserById(XBoxOneStorageUtils.UserId);
        
        if (currentUser != null)
        {
            OnScreenLog.Instance.Add(string.Format("AchievementsController. User Id = {0}, User UID = {1}, AchievementId = {2}", currentUser.Id, currentUser.UID, trophyIndex));
            AchievementsManager.UpdateAchievementAsync(currentUser.Id, currentUser.UID, trophyIndex.ToString(), 100, UpdateAchievementCallback);
        }
        else
        {
            OnScreenLog.Instance.Add(string.Format("AchievementsController. Can't send AchievementId = {0} because User Id = {1} doesn't sign in", trophyIndex, XBoxOneStorageUtils.UserId));
        }
    }
    private void OnUnlockNotifications(AchievementUnlockedEventArgs payload)
    {

        OnScreenLog.Instance.Add("Achievement notice: " + payload.AchievementId + " by user: " + payload.XboxUserId);
    }
    
    public void DisconnectStorage()
    {
        Debug.LogErrorFormat("DisconnectStorage. UserId: {0}", XBoxOneStorageUtils.UserId);
        XBoxOneStorageUtils.UserId = -1;
        Infrastructure.Instance.Storage.Cleanup();
    }
    
    public bool ConnectStorage(int signInUserId)
    {
        Debug.LogErrorFormat("ConnectStorage. UserId: {0}, Previous UserId: {1}", signInUserId, XBoxOneStorageUtils.UserId);
        if (signInUserId != XBoxOneStorageUtils.UserId)
        {
            XBoxOneStorageUtils.UserId = signInUserId;
            Infrastructure.Instance.Storage.Cleanup();
            Infrastructure.Instance.Storage.Init();
            PlayerPrefs.ReLoad();
            return true;
        }

        return false;
    }
}
#endif