﻿using System.Collections;
using System.Linq;
using Rewired;
#if UNITY_SWITCH
using Rewired.Platforms.Switch;
#endif
using UnityEngine;

public class SwitchManager : BaseManager
{
    private Player player;
    
#if UNITY_SWITCH && !UNITY_EDITOR
    private nn.account.Uid userId;
    private nn.account.Nickname userName;
#endif
    
    public override void Init()
    {
#if UNITY_SWITCH && !UNITY_EDITOR
        nn.account.Account.Initialize();
        var userHandle = new nn.account.UserHandle();
        nn.account.Account.TryOpenPreselectedUser(ref userHandle);
        nn.account.Account.GetUserId(ref userId, userHandle);
        nn.account.Account.GetNickname(ref userName, userId);

#endif

        ReInput.ControllerDisconnectedEvent += OnControllerDisconnected;
       // ReInput.ControllerConnectedEvent += OnControllerConnected;
    }

    private void Start()
    {
        player = ReInput.players.GetPlayer(0);
    }
    public override void Cleanup()
    {
        ReInput.ControllerDisconnectedEvent -= OnControllerDisconnected;
        //ReInput.ControllerConnectedEvent -= OnControllerConnected;
    }
#if UNITY_SWITCH && !UNITY_EDITOR
    public nn.account.Uid GetUserId()
    {
        return userId;
    }

    public override string GetUserName()
    {
        return userName.name;
    }

    public override string GetUserID()
    {
        return userId.ToString();
    }
#endif
    

    //private static void OnControllerDisconnected(ControllerStatusChangedEventArgs args)
    //{
        
   /// }

    private static void OnControllerConnected(ControllerStatusChangedEventArgs args)
    {
#if UNITY_SWITCH && !UNITY_EDITOR
        NpadId[] npadIds = {NpadId.No1, NpadId.No2, NpadId.No3, NpadId.No4, NpadId.No5, NpadId.No6, NpadId.No7, NpadId.No8};

        if (npadIds.Select(SwitchInput.Npad.GetNpad).Any(npad => npad.IsConnected()))
        {
            var options = new ControllerAppletOptions {playerCountMax = 1, singlePlayerMode = true};
            UnityEngine.Switch.Applet.Begin();
            SwitchInput.ControllerApplet.Show(options);
            UnityEngine.Switch.Applet.End();
        }
#endif
    }
    
    
   private bool checkCoroutineStarted;
    
    // This function will be called when a controller is fully disconnected
    // You can get information about the controller that was disconnected via the args parameter
    void OnControllerDisconnected(ControllerStatusChangedEventArgs args)
    {
        Debug.Log("A controller was disconnected! Name = " + args.name + " Id = " + args.controllerId + " Type = " + args.controllerType);
        if (!checkCoroutineStarted)
        {
            Debug.Log("[SWITCHAPPLET] OnControllerDisconnected Launched ShowControllerSupport");
            StartCoroutine(ShowControllerSupport());
        }
    }

    IEnumerator ShowControllerSupport()
    {
        yield return new WaitForEndOfFrame();
#if UNITY_SWITCH && !UNITY_EDITOR
		ControllerAppletOptions options = new ControllerAppletOptions();
        options.playerCountMax = 1;
        options.singlePlayerMode = true;
        UnityEngine.Switch.Applet.Begin();
        SwitchInput.ControllerApplet.Show(options);
        UnityEngine.Switch.Applet.End();
#endif
        
        yield break;

    }


    private bool checkIfHalfConnectedDual()
    {
        // Loop through all Joysticks in the Player
        for (int i = 0; i < player.controllers.joystickCount; i++)
        {
            Joystick joystick = player.controllers.Joysticks[i];

#if UNITY_SWITCH
            // Get the Switch Gamepad Extension from the Joystick
            DualJoyConExtension ext = joystick.GetExtension<DualJoyConExtension>();
            {
                if (ext != null)
                {
                    if (ext.npadStyle == Rewired.Platforms.Switch.NpadStyle.JoyConDual)
                    {
                        if (!ext.IsLeftJoyConConnected() || !ext.IsRightJoyConConnected())
                        {
                            // not enough devices connected
                            Debug.Log("[SWITCHAPPLET] checkIfHalfConnectedDual Disconnecting");
                            ext.Disconnect();
                            return true;
                        }
                    }
                }
            }
#endif //UNITY_SWITCH
        }
        return false;
    }



    // Update is called once per frame
    void Update ()
    {
#if !UNITY_EDITOR
        checkIfHalfConnectedDual();

        // if no joyctick connected -- call the applet

        if (!IsAnyJoystickConnected() && !checkCoroutineStarted)
        {
            StartCoroutine(checkAndLaunchApplet());
            checkCoroutineStarted = true;
            Debug.Log("[SWITCHAPPLET] Launched Coroutine");
        }
#endif
    }

    bool IsAnyJoystickConnected()
    {
        for (int i = 0; i < player.controllers.joystickCount; i++)
        {
            Joystick joystick = player.controllers.Joysticks[i];
            if (joystick!=null && joystick.isConnected)
            {
                return true;
            }
        }
        return false;
    }

    IEnumerator checkAndLaunchApplet()
    {
        int count = 0;
        Joystick joystick = ReInput.controllers.GetJoystick(0);
        while (!IsAnyJoystickConnected())
        {
            yield return new WaitForEndOfFrame();
            if (!IsAnyJoystickConnected())
            {
                count++;
                if (count > 15)
                {
                    Debug.Log("[SWITCHAPPLET] checkAndLaunchApplet ShowControllerSupport");
                    StartCoroutine(ShowControllerSupport());
                    yield return new WaitForSeconds(1);
                    if (!IsAnyJoystickConnected())
                    {
                        Debug.Log("[SWITCHAPPLET] after ShowControllerSupport : no controller connected");
                    }
                    else
                    {
                        Debug.Log("[SWITCHAPPLET] after ShowControllerSupport : Controller is Connected");
                    }
                    count = 0;
                }
                else
                {
                    Debug.Log("[SWITCHAPPLET] Scanning count "+count);
                }
            }
            else
            {
                Debug.Log("[SWITCHAPPLET] in check : Controller is detected");
            }
        }
        checkCoroutineStarted = false;
        Debug.Log("[SWITCHAPPLET] exiting ShowControllerSupport");
    }
    
}
