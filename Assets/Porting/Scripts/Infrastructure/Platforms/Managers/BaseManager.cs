﻿using System;
using UnityEngine;

public class BaseManager : MonoBehaviour
{
    public Action onUserSignOut;
    public Action onPause;
    public Action onUnPause;
    public virtual void Start()
    {
        //Init();
    }
    
//    public virtual void OnDestroy()
//    {
//        Cleanup();
//    }

    public virtual void Init()
    {
        
    }

    public virtual void Cleanup()
    {
        
    }

    public virtual string GetUserName()
    {
        return "269 Lalalend";
    }

    public virtual string GetUserID()
    {
        return "F344675B65CF490C3";
    }
    
    public virtual void UnlockingAchievement(string id)
    {
        
    }
}
