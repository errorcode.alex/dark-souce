﻿
using System.Linq;
using Rewired;
#if UNITY_PS4
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;


using UnityEngine.PS4;

public class PS4Manager : BaseManager
{
    public static PS4Manager Instance;
    
    private Sony.NP.InitResult initResult;
    private Sony.NP.UserProfiles.LocalUsers users = new Sony.NP.UserProfiles.LocalUsers();
    private int primaryUserId;
    
    public override void Init()
    {
        Instance = this;
#if !UNITY_EDITOR
        InitialiseNpToolkit();
#endif
        ReInput.ControllerDisconnectedEvent += OnControllerDisconnected;
        ReInput.ControllerConnectedEvent += OnControllerConnected;
        ReInput.ControllerPreDisconnectEvent += OnControllerPreDisconnect;
    }

    private void InitialiseNpToolkit()
    {
        Sony.NP.Main.OnAsyncEvent += Main_OnAsyncEvent;
        
        Sony.NP.InitToolkit init = new Sony.NP.InitToolkit();
        
        init.contentRestrictions.ApplyContentRestriction = false;

        init.threadSettings.affinity = Sony.NP.Affinity.AllCores;

        init.memoryPools.JsonPoolSize = 6 * 1024 * 1024;
        init.memoryPools.SslPoolSize *= 4;
        init.memoryPools.MatchingSslPoolSize *= 4;
        init.memoryPools.MatchingPoolSize *= 4;

        init.SetPushNotificationsFlags(Sony.NP.PushNotificationsFlags.NewGameDataMessage |
                                       Sony.NP.PushNotificationsFlags.NewInvitation |
                                       Sony.NP.PushNotificationsFlags.UpdateBlockedUsersList |
                                       Sony.NP.PushNotificationsFlags.UpdateFriendPresence |
                                       Sony.NP.PushNotificationsFlags.UpdateFriendsList);
        
        FindUsers();
        try
        {
            initResult = Sony.NP.Main.Initialize(init);
            RegisterTrophyPack();
            if (initResult.Initialized)
            {
                OnScreenLog.Instance.Add("NpToolkit Initialized ");
                OnScreenLog.Instance.Add("Plugin SDK Version : " + initResult.SceSDKVersion);
                OnScreenLog.Instance.Add("Plugin DLL Version : " + initResult.DllVersion);
            }
            else
            {
                OnScreenLog.Instance.Add("NpToolkit not initialized ");
            }
        }
        catch (Sony.NP.NpToolkitException e)
        {
            OnScreenLog.Instance.Add("Exception During Initialization : " + e.ExtendedMessage);
        }
#if UNITY_EDITOR
        catch (DllNotFoundException e)
        {
            OnScreenLog.Instance.Add("Missing DLL Expection : " + e.Message);
            OnScreenLog.Instance.Add("The sample APP will not run in the editor.");
        }
#endif
    }
    
    public override void Cleanup()
    {
        ReInput.ControllerDisconnectedEvent -= OnControllerDisconnected;
        ReInput.ControllerConnectedEvent -= OnControllerConnected;
        ReInput.ControllerPreDisconnectEvent -= OnControllerPreDisconnect;
    }
    
    private static void OnControllerDisconnected(ControllerStatusChangedEventArgs args)
    {
        if (ReInput.controllers.Controllers.Any(controller => controller.isConnected && controller.type == ControllerType.Joystick))
            return;
        
        if(Instance.onPause != null)
            Instance.onPause.Invoke();
    }

    private static void OnControllerConnected(ControllerStatusChangedEventArgs args)
    {
        if(Instance.onUnPause != null)
            Instance.onUnPause.Invoke();
    }

    private static void OnControllerPreDisconnect(ControllerStatusChangedEventArgs args)
    {
        Debug.LogError("OnControllerPreDisconnect");
        //Instance.messagebox = Instantiate(Instance.prefabMessagebox);
    }
    
    private void FindUsers()
    {
        try
        {
            Sony.NP.UserProfiles.GetLocalUsers(users);
        }
        catch (Sony.NP.NpToolkitException)
        {
            OnScreenLog.Instance.Add("catch Sony.NP.NpToolkitException");
            // This means that one or more of the user has an error code associated with them. This might mean they
            // are not signed in or are not signed up to an online account.
        }

        primaryUserId = -1;
        // Use the account ids to fill in the request
        for (var i = 0; i < users.LocalUsersIds.Length && primaryUserId < 0; i++)
        {
            if (users.LocalUsersIds[i].UserId.Id != Sony.NP.Core.UserServiceUserId.UserIdInvalid &&
                users.LocalUsersIds[i].AccountId.Id != 0)
            {
                primaryUserId = i;
            }
        }

        OnScreenLog.Instance.Add("Find Users complete");
    }

    private Sony.NP.Core.UserServiceUserId GetPrimaryUserId()
    {
        return primaryUserId < 0
            ? Sony.NP.Core.UserServiceUserId.UserIdInvalid
            : users.LocalUsersIds[primaryUserId].UserId;
    }

    public override string GetUserName()
    {
        return PS4Input.GetUsersDetails(0).userName;
    }
    
    private void RegisterTrophyPack()
    {
        try
        {
            Sony.NP.Trophies.RegisterTrophyPackRequest request = new Sony.NP.Trophies.RegisterTrophyPackRequest
            {
#if UNITY_EU
                ServiceLabel = 0,
#endif
#if UNITY_US
                ServiceLabel = 1,
#endif
                UserId = GetPrimaryUserId()
            };

            Sony.NP.Core.EmptyResponse response = new Sony.NP.Core.EmptyResponse();

            // Make the async call which returns the Request Id 
            int requestId = Sony.NP.Trophies.RegisterTrophyPack(request, response);
            OnScreenLog.Instance.Add("RegisterTrophyPack Async : Request Id = " + requestId);
        }
        catch (Sony.NP.NpToolkitException e)
        {
            OnScreenLog.Instance.Add("Exception : " + e.ExtendedMessage);
        }
    }

    public override void UnlockingAchievement(string id)
    {
        var trophyIndex = int.Parse(id, CultureInfo.InvariantCulture);
        OnScreenLog.Instance.Add("UnlockTrophy. Trophy Id = " + trophyIndex);

#if !UNITY_EDITOR
        try
        {
            Sony.NP.Trophies.UnlockTrophyRequest request = new Sony.NP.Trophies.UnlockTrophyRequest
            {
#if UNITY_EU
                ServiceLabel = 0,
#endif
#if UNITY_US
                ServiceLabel = 1,
#endif
                TrophyId = trophyIndex,
                UserId = GetPrimaryUserId()
            };

            Sony.NP.Core.EmptyResponse response = new Sony.NP.Core.EmptyResponse();

            // Make the async call which returns the Request Id 
            int requestId = Sony.NP.Trophies.UnlockTrophy(request, response);
            OnScreenLog.Instance.Add("UnlockTrophy. Request Id = " + requestId);
        }
        catch (Sony.NP.NpToolkitException e)
        {
            OnScreenLog.Instance.Add("Exception : " + e.ExtendedMessage);
        }
#endif
    }

    // NOTE : This is called on the "Sony NP" thread and is independent of the Behaviour update.
    // This thread is created by the SonyNP.dll when NpToolkit2 is initialised.
    private void Main_OnAsyncEvent(Sony.NP.NpCallbackEvent callbackEvent)
    {
        OnScreenLog.Instance.Add("Event: Service = (" + callbackEvent.Service + ") : API Called = (" 
                        + callbackEvent.ApiCalled + ") : Request Id = (" + callbackEvent.NpRequestId + 
                        ") : Calling User Id = (" + callbackEvent.UserId + ")");
        HandleAsyncEvent(callbackEvent);
    }
    
    private void HandleAsyncEvent(Sony.NP.NpCallbackEvent callbackEvent)
    {
        try
        {
            if (callbackEvent.Response != null)
            {
                //We got an error response 
                if (callbackEvent.Response.ReturnCodeValue < 0)
                {
                    //NP_TROPHY_ERROR_TROPHY_ALREADY_UNLOCKED
                    string message = string.Format("HandleAsyncEvent. Error response : {0}",
                        callbackEvent.Response.ConvertReturnCodeToString(callbackEvent.ApiCalled));
                    Debug.LogError(message);
                    OnScreenLog.Instance.Add(message);
                }
                else
                {
                    //The callback of the event is a trophyUnlock event
                    if (callbackEvent.ApiCalled == Sony.NP.FunctionTypes.TrophyUnlock)
                    {
                        OnScreenLog.Instance.Add("HandleAsyncEvent. Trophy Unlock : " + 
                                        callbackEvent.Response.ConvertReturnCodeToString(callbackEvent.ApiCalled));
                    }
                }
            }
            else
            {
                OnScreenLog.Instance.Add("HandleAsyncEvent. Response is null");
            }
        }
        catch (Sony.NP.NpToolkitException e)
        {
            string message = string.Format("HandleAsyncEvent. Exception: {0}", e.ExtendedMessage);
            Debug.LogError(message);
            OnScreenLog.Instance.Add(message);
        }
    }
}
#endif