﻿#if UNITY_XBOXONE
using System;
using System.Text;
using Storage;
using Users;
using UnityPluginLog;
using UnityEngine;
using System.Collections;

public sealed class XBoxOneStorage : IStorage
{
    private const int bufferSize = 1024 * 1024;
    private const int threadMillisecondsTimeout = 1;
    private const string CONTAINER_NAME = "GameSaveContainer";
    private ConnectedStorage connectedStorage = null;
    private ConnectedStorageCreationFlags flags = 0;
    private int userId = -1;
    private bool didPass = true;

    private bool shouldSuspend = false;
    private bool readyToSuspend = false;
    private bool waitForConnectedStorageCreated = false;
    private bool wasInitialized = false;

    #region implementation_IStorage

    public void Init()
    {
        if (wasInitialized)
        {
            OnScreenLog.Instance.Add("XBoxOneStorage already successfully initialized");
            return;
        }

// only one call
        PluginLogManager.Create();
        PluginLogManager.OnLog += OnLog;

        OnScreenLog.Instance.Add("XBoxOneStorage. Init. IsSomeoneSignedIn = " + UsersManager.IsSomeoneSignedIn + " UserId = " + XBoxOneStorageUtils.UserId + " \n");
        OnScreenLog.Instance.Add("Initializing Systems\n");
        OnScreenLog.Instance.Add("\n");

        if (!UsersManager.IsSomeoneSignedIn || XBoxOneStorageUtils.UserId < 0)
        {
            OnScreenLog.Instance.Add("No one signed in. This storage requires someone be signed in to function properly.\n");
        }
        else
        {
            OnScreenLog.Instance.Add("Starting up...\n");
            OnScreenLog.Instance.Add("NOTE: You must have someone signed in, and you must have access to\n");
            OnScreenLog.Instance.Add("NOTE: the titles SCID (including write access) for this storage to work.\n");
            OnScreenLog.Instance.Add("NOTE: You should update the AppXManifest with your SCID to see a working version of this demo\n");
            OnScreenLog.Instance.Add("\n");

            // Suspend/Resume with storage requires that you dispose of the storage object and create a new connection on resume. Failing to do so can
            // result in use of a storage object that has gone invalid. This can be a cause of problems for some developers.
            //XboxOnePLM.OnSuspendingEvent += OnSuspendingEvent;
            //XboxOnePLM.OnResumingEvent += OnResumingEvent;

            CreateStorage();
        }
    }

    public void Update()
    {
        
    }

    public IEnumerator SaveAsync(string fileName, byte[] content, Action onComplete = null)
    {
        if (content == null || content.Length == 0)
        {
            OnScreenLog.Instance.Add("Save. Filename = " + fileName + " Content null or empty. Skip saving");
            yield break;
        }

        OnScreenLog.Instance.Add("Save. Filename = " + fileName + " waitForConnectedStorageCreated = " + waitForConnectedStorageCreated);
        
        while (waitForConnectedStorageCreated)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);
        
        if (connectedStorage == null)
        {
            OnScreenLog.Instance.Add("Save. ConnectedStorage doesn't created");
            yield break;
        }

        OnScreenLog.Instance.Add("Save. Start async operation. Filename = " + fileName);

        var dataToSave = DataMap.Create();

        // Update our data map and write it out to our container.
        dataToSave.AddOrReplaceBuffer(fileName, content);
        var asyncOp = connectedStorage.SubmitUpdatesAsync(dataToSave, null, OnSaveDone);
        
        while (!asyncOp.IsComplete)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        OnScreenLog.Instance.Add("Save. Complete async operation. Filename = " + fileName + " Result = " + asyncOp.Success);
        
        if(onComplete != null)
            onComplete.Invoke();
        yield return null;
    }
    
    public IEnumerator SaveAsync(string fileName, string content, Action onComplete = null)
    {
        var bytes = Encoding.UTF8.GetBytes(content);
        yield return SaveAsync(fileName, bytes, onComplete);
    }
    
    public IEnumerator ReadAsync(string fileName, Action<byte[]> onComplete)
    {
        OnScreenLog.Instance.Add("Read. Filename = " + fileName + "  waitForConnectedStorageCreated = " +
                                 waitForConnectedStorageCreated);

        while (waitForConnectedStorageCreated)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        if (connectedStorage == null)
        {
            OnScreenLog.Instance.Add("Read. ConnectedStorage doesn't created");
            if(onComplete != null)
                onComplete.Invoke(new byte[0]);
            yield break;
        }

        OnScreenLog.Instance.Add("Read. Start async operation. Filename = " + fileName);
        var asyncOp = connectedStorage.GetAsync(new string[] {fileName}, OnReadDone);

        while (!asyncOp.IsComplete)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        OnScreenLog.Instance.Add("Read. Complete async operation. Filename = " + fileName + " Result = " + asyncOp.Success);
        if (asyncOp.Success)
        {
            byte[] buffer = asyncOp.Data.GetBuffer(fileName);
            //OnScreenLog.Instance.Add("Read. BufferInfo: Length: [" + buffer.Length + "] 0=" + buffer[0] + " 99=" +
            //                         buffer[99] + "\n");

            if(onComplete != null)
                onComplete.Invoke(buffer);
            yield break;
        }

        if(onComplete != null)
            onComplete.Invoke(new byte[0]);

        yield return null;
    }
    
    public IEnumerator ReadTextAsync(string fileName, Action<string> onComplete)
    {
        OnScreenLog.Instance.Add("Read. Filename = " + fileName + "  waitForConnectedStorageCreated = " +
                                 waitForConnectedStorageCreated);

        while (waitForConnectedStorageCreated)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        if (connectedStorage == null)
        {
            OnScreenLog.Instance.Add("Read. ConnectedStorage doesn't created");
            if(onComplete != null)
                onComplete.Invoke(string.Empty);
            yield break;
        }

        OnScreenLog.Instance.Add("Read. Start async operation. Filename = " + fileName);
        var asyncOp = connectedStorage.GetAsync(new string[] {fileName}, OnReadDone);

        while (!asyncOp.IsComplete)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        OnScreenLog.Instance.Add("Read. Complete async operation. Filename = " + fileName + " Result = " + asyncOp.Success);
        if (asyncOp.Success)
        {
            byte[] buffer = asyncOp.Data.GetBuffer(fileName);
            //OnScreenLog.Instance.Add("Read. BufferInfo: Length: [" + buffer.Length + "] 0=" + buffer[0] + " 99=" +
            //                         buffer[99] + "\n");

            if(onComplete != null)
                onComplete.Invoke(Encoding.UTF8.GetString(buffer));
            yield break;
        }

        if(onComplete != null)
            onComplete.Invoke(string.Empty);

        yield return null;
    }
    
    public IEnumerator FileExistsAsync(string fileName, Action<bool> onExists)
    {
        OnScreenLog.Instance.Add("FileExists. Start async operation. Filename = " + fileName +
                        "  waitForConnectedStorageCreated = " + waitForConnectedStorageCreated);

        while (waitForConnectedStorageCreated)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        if (connectedStorage == null)
        {
            OnScreenLog.Instance.Add("FileExists. ConnectedStorage doesn't created");
            
            if(onExists != null)
                onExists.Invoke(false);
            
            yield break;
        }

        OnScreenLog.Instance.Add("FileExists. Start async operation. Filename = " + fileName);

        var asyncOp = connectedStorage.GetAsync(new string[] {fileName}, OnFileExistDone);
        while (!asyncOp.IsComplete)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        OnScreenLog.Instance.Add("FileExists. Complete async operation. Filename = " + fileName + " Result = " +
                        asyncOp.Success);

        if(onExists != null)
            onExists.Invoke(asyncOp.Success);
    
        yield return null;
    }

    public IEnumerator DeleteFileAsync(string fileName, Action onComplete = null)
    {
        OnScreenLog.Instance.Add("DeleteFile. Start async operation. Filename = " + fileName + "  waitForConnectedStorageCreated = " + waitForConnectedStorageCreated);

        while (waitForConnectedStorageCreated)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        if (connectedStorage == null)
        {
            OnScreenLog.Instance.Add("DeleteFile. ConnectedStorage doesn't created");
            yield break;
        }

        OnScreenLog.Instance.Add("DeleteFile. Start async operation. Filename = " + fileName);

        var asyncOp = connectedStorage.DeleteAsync(new string[] {fileName}, OnDeleteFileDone);
        
        while (!asyncOp.IsComplete)
            yield return new WaitForSeconds(threadMillisecondsTimeout / 10000);

        OnScreenLog.Instance.Add("DeleteFile. Complete async operation. Filename = " + fileName + " Result = " + asyncOp.Success);
        
        if(onComplete != null)
            onComplete.Invoke();
        yield return null;
    }

    public void Save(string fileName, byte[] content)
    {
        throw new NotImplementedException();
    }

    public void Save(string fileName, string content)
    {
        throw new NotImplementedException();
    }

    public byte[] Read(string fileName)
    {
        throw new NotImplementedException();
    }

    public string ReadText(string fileName)
    {
        throw new NotImplementedException();
    }

    public bool FileExists(string fileName)
    {
        throw new NotImplementedException();
    }

    public void DeleteFile(string fileName)
    {
        throw new NotImplementedException();
    }

    #endregion

    private void OnReadDone(ContainerContext storage, GetDataMapViewAsyncOp op, DataMapView view)
    {
        // If we are being asked to suspend do not try to issue more operations
        // our storage object will be invalid when we return from suspension
        if (SuspendCheck())
            return;

        bool ok = op.Success;
        didPass = didPass && ok;

        OnScreenLog.Instance.Add("Read. [" + (ok ? "PASS" : "FAIL") + "] GETASYNC: \"NewBuffer\": [" +
                        (op.Success ? "Exists" : "NonExistent") + "] [0x" + op.Result.ToString("X") + "]\n");
    }

    private void OnSaveDone(ContainerContext storage, SubmitDataMapUpdatesAsyncOp op)
    {
        // If we are being asked to suspend do not try to issue more operations
        // our storage object will be invalid when we return from suspension
        if (SuspendCheck())
            return;

        bool ok = op.Success && op.Status == ConnectedStorageStatus.SUCCESS;
        didPass = didPass && ok;
        // Save completed, lets make sure it worked.
        OnScreenLog.Instance.Add("Save. [" + (ok ? "PASS" : "FAIL") + "] SAVE: Completed RESULT: [" + op.Result +
                        "] STATUS: [" + op.Status.ToString() + "]\n");
    }

    private void OnFileExistDone(ContainerContext storage, GetDataMapViewAsyncOp op, DataMapView view)
    {
        // If we are being asked to suspend do not try to issue more operations
        // our storage object will be invalid when we return from suspension
        if (SuspendCheck())
            return;

        bool ok = op.Success;
        didPass = didPass && ok;
        OnScreenLog.Instance.Add("FileExists. [" + (ok ? "PASS" : "FAIL") + "] BADREAD: [" +
                        (op.Success ? "Exists" : "NonExistent") + "] [0x" + op.Result.ToString("X") + "]\n");
    }

    private void OnDeleteFileDone(ContainerContext storage, SubmitDataMapUpdatesAsyncOp op)
    {
        // If we are being asked to suspend do not try to issue more operations
        // our storage object will be invalid when we return from suspension
        if (SuspendCheck())
            return;

        bool ok = op.Success;
        didPass = didPass && ok;
        OnScreenLog.Instance.Add("DeleteFile. [" + (ok ? "PASS" : "FAIL") + "] DELETE: \"NewBuffer\"\n");
    }
    

    private void OnLog(LogChannels channels, string message)
    {
        if (channels == LogChannels.kLogErrors || channels == LogChannels.kLogExceptions)
            OnScreenLog.Instance.Add(message);
    }

    // Use this for initialization
    private void CreateStorage()
    {
        int previousUserId = userId;
        userId = XBoxOneStorageUtils.UserId;
        flags = 0;
        didPass = true;
        waitForConnectedStorageCreated = true;

        OnScreenLog.Instance.Add(string.Format(
            "CreateStorage. SignedIn UserId: {0}, Previous UserId: {1}, ConnectedStorageCreationFlags: {2}", userId,
            previousUserId, flags));

        // This is async, I could poll on the async op or I could just use the callback
        // this little object chains through various operations using the callback.
        ConnectedStorage.CreateAsync(userId, CONTAINER_NAME, OnConnectedStorageCreated, flags);
    }

    public IEnumerator OnSuspend(Action callback)
    {
        OnScreenLog.Instance.Add(string.Format("OnSuspend. Started. ReadyToSuspend: {0}", readyToSuspend));

        shouldSuspend = true;
        /*
        while (!readyToSuspend)
        {
            yield return null;
        }
        */
        OnScreenLog.Instance.Add("OnSuspend. Dispose ConnectedStorage");

        // Forcibly dispose of the title storage object
        // it maintains a connection to live which will
        // become invalid after we return.
        if (connectedStorage != null)
        {
            connectedStorage.Dispose();
            connectedStorage = null;
            OnScreenLog.Instance.Add("XBoxOneStorage. Dispose. Completed");
        }

        if (callback != null)
            callback();

        OnScreenLog.Instance.Add("OnSuspend. Completed");

        yield break;
    }

    private bool SuspendCheck()
    {
        if (shouldSuspend)
        {
            readyToSuspend = true;
            return true;
        }

        return false;
    }

    public void OnResume(int signedUserId, double secondsSuspended)
    {
        OnScreenLog.Instance.Add(string.Format(
            "OnResumingEvent. SignedIn UserId: {0}, Previous UserId: {1}, Seconds suspended {2}", signedUserId, userId, secondsSuspended));
        
        shouldSuspend = false;
        readyToSuspend = false;
        didPass = true;

        // This is async, I could poll on the async op or I could just use the callback
        // this little object chains through various operations using the callback.
        User user = UsersManager.FindUserById(signedUserId);
        if (user != null)
            CreateStorage();

        OnScreenLog.Instance.Add(string.Format("[Connected Storage] OnResume. UserId: {0}, ", signedUserId));
    }

    public void Cleanup()
    {
        OnScreenLog.Instance.Add("XBoxOneStorage. Cleanup. Started");

        PluginLogManager.OnLog -= OnLog;

        //XboxOnePLM.OnSuspendingEvent -= OnSuspendingEvent;
        //XboxOnePLM.OnResumingEvent -= OnResumingEvent;

        if (connectedStorage != null)
        {
            // We could leave this up to the GC, but it's much better to clean this up manually
            // like we are doing below.
            connectedStorage.Dispose();
            connectedStorage = null;
            OnScreenLog.Instance.Add("XBoxOneStorage. Dispose. Completed");
        }

        wasInitialized = false;

        OnScreenLog.Instance.Add("XBoxOneStorage. Cleanup. Completed");
    }

    private void OnConnectedStorageCreated(ConnectedStorage storage, CreateConnectedStorageOp op)
    {
        OnScreenLog.Instance.Add("OnConnectedStorageCreated");
        // If we are being asked to suspend do not try to issue more operations
        // our storage object will be invalid when we return from suspension
        if (SuspendCheck())
            return;

        wasInitialized = true;
        waitForConnectedStorageCreated = false;
        if (op.Success)
        {
            // Keep this around so we can actually run operations.
            connectedStorage = storage;
            if (userId == -1)
                OnScreenLog.Instance.Add("[Machine storage created]\n");
            else
                OnScreenLog.Instance.Add(flags == 0 ? "[User ConnectedStorage created]\n" : "[On Demand User ConnectedStorage created]\n");
        }
        else
        {
            didPass = false;
            OnScreenLog.Instance.Add("Machine storage failed initialization RESULT: [0x" + op.Result.ToString("X") + "]\n");
        }
    }
}
#endif
