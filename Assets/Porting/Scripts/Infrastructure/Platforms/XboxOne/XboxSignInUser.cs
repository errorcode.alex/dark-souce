﻿using System;
using UnityEngine.Events;
using Consoles;
using UnityEngine;
#if UNITY_XBOXONE
using Users;
#endif

public class XboxSignInUser : MonoBehaviour
{
    private bool showingAccountPicker = false;
#if UNITY_XBOXONE
    private XboxOneManager xboxOneManager;
#endif

    public Action<bool> OnSignIn;
    public Action<bool> OnReloadProfile;

    private void Awake()
    {

    }

    void Start()
    {
#if UNITY_EDITOR
        
        return;
#endif
#if UNITY_XBOXONE
        xboxOneManager = Infrastructure.Instance.Manager as XboxOneManager;
        User selectedUser = UsersManager.FindUserById(XBoxOneStorageUtils.UserId);
        if (selectedUser != null)
        {
            if (XBoxOneStorageUtils.UserId == selectedUser.Id)
            {
                XBoxOneStorageUtils.GameDisplayName = selectedUser.GameDisplayName;
            }
        }
#endif
    }


    public void UpdateAnyButton()
    {
        
#if UNITY_XBOXONE
        if (showingAccountPicker)
            return;
        
        int userId = -1;
        uint gamepadIndex = 0;
        // Which gamepad was it pressed on?
        XboxOneKeyCode keyCode;
        
        if (XboxOneInputHelpers.GetButtonAnyDown(out keyCode))// && !authorized)
        {
            gamepadIndex = XboxOneInput.GetGamepadIndexFromGamepadButton(keyCode);
            User user = XboxOneInputHelpers.GetUserWhenPlayerPressButton(gamepadIndex);
            userId = user != null ? user.Id : -1;

            Debug.LogFormat("UIPopupUserSignOut. OnContinueButtonClick. UserId: {0}, Previous UserId: {1}, GamepadIndex: {2}",
                userId, XBoxOneStorageUtils.UserId, gamepadIndex);
            
            if(userId == -1)
            {
                Debug.Log("UIPopupUserSignOut. OnContinueButtonClick. User not payired yet - request sign-in");

                // Not paired yet - request sign-in
                UsersManager.RequestSignIn(AccountPickerOptions.AllowGuests, gamepadIndex);
                showingAccountPicker = true;
                // Register callback for Account Picker closing
                UsersManager.OnSignInComplete += OnSignInComplete;
                typeA = true;
            }
            else
            {
                User selectedUser = UsersManager.FindUserById(userId);
                if(selectedUser != null)
                    XBoxOneStorageUtils.GameDisplayName = selectedUser.GameDisplayName;

                if (xboxOneManager.ConnectStorage(userId))
                {
                    if(OnReloadProfile != null)
                        OnReloadProfile.Invoke(true);
                }

                if(OnSignIn != null)
                    OnSignIn.Invoke(true);
            }
        }
#endif
    }

    public void UpdateChangeUser()
    {
#if UNITY_XBOXONE
        if (showingAccountPicker)
            return;
        
        int userId = -1;
        uint gamepadIndex = 0;
        XboxOneKeyCode keyCode;
        
        if (XboxOneInputHelpers.GetButtonYDown(out keyCode))// && authorized)
        {
            gamepadIndex = XboxOneInput.GetGamepadIndexFromGamepadButton(keyCode);
            User user = XboxOneInputHelpers.GetUserWhenPlayerPressButton(gamepadIndex);
            userId = user != null ? user.Id : -1;

            Debug.LogFormat("ChangeUser. OnContinueButtonClick. UserId: {0}, Previous UserId: {1}, GamepadIndex: {2}",
                userId, XBoxOneStorageUtils.UserId, gamepadIndex);
        
            Debug.Log("UIPopupUserSignOut. OnContinueButtonClick. User not payired yet - request sign-in");

            UsersManager.RequestSignIn(AccountPickerOptions.AllowGuests, gamepadIndex);
            showingAccountPicker = true;
            
            UsersManager.OnSignInComplete += OnSignInComplete;
            typeY = true;
        }
#endif
    }

    private bool typeA;
    private bool typeY;
    private void OnSignInComplete(int resultType, int userId)
    {
#if UNITY_XBOXONE
        Debug.LogFormat("UIPopupUserSignOut. OnSignInComplete. ResultType: {0}, UserId: {1}, Previous UserId: {2}",
            resultType, userId, XBoxOneStorageUtils.UserId);
        
        UsersManager.OnSignInComplete -= OnSignInComplete;
        
        if (userId == -1)
        {
            showingAccountPicker = false;
            typeA = false;
            typeY = false;
            return;
        }
        
        User selectedUser = UsersManager.FindUserById(userId);
        if (xboxOneManager.ConnectStorage(userId))
        {
            if (typeA)
            {
                if(OnReloadProfile != null)
                    OnReloadProfile.Invoke(true);
            }

            if (typeY)
            {
                if(OnReloadProfile != null)
                    OnReloadProfile.Invoke(false);
            }

            if (selectedUser != null)
            {
                if (XBoxOneStorageUtils.UserId == userId)
                {
                    XBoxOneStorageUtils.GameDisplayName = selectedUser.GameDisplayName;
                    
                    if (typeA)
                    {
                        if(OnSignIn != null)
                            OnSignIn.Invoke(true);
                    }

                    if (typeY)
                    {
                        if(OnSignIn != null)
                            OnSignIn.Invoke(false);
                    }
                    
                }
            }
        }
        showingAccountPicker = false;
        typeA = false;
        typeY = false;
#endif
    }
}