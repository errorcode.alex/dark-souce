﻿#if UNITY_XBOXONE
using System.Collections;
using UnityEngine;
using Users;

public static class XboxOneInputHelpers
{
        public static readonly XboxOneKeyCode[] aButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonA,
            XboxOneKeyCode.Gamepad2ButtonA,
            XboxOneKeyCode.Gamepad3ButtonA,
            XboxOneKeyCode.Gamepad4ButtonA,
            XboxOneKeyCode.Gamepad5ButtonA,
            XboxOneKeyCode.Gamepad6ButtonA,
            XboxOneKeyCode.Gamepad7ButtonA,
            XboxOneKeyCode.Gamepad8ButtonA
        };

        public static readonly XboxOneKeyCode[] bButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonB,
            XboxOneKeyCode.Gamepad2ButtonB,
            XboxOneKeyCode.Gamepad3ButtonB,
            XboxOneKeyCode.Gamepad4ButtonB,
            XboxOneKeyCode.Gamepad5ButtonB,
            XboxOneKeyCode.Gamepad6ButtonB,
            XboxOneKeyCode.Gamepad7ButtonB,
            XboxOneKeyCode.Gamepad8ButtonB
        };
        
        public static readonly XboxOneKeyCode[] yButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonY,
            XboxOneKeyCode.Gamepad2ButtonY,
            XboxOneKeyCode.Gamepad3ButtonY,
            XboxOneKeyCode.Gamepad4ButtonY,
            XboxOneKeyCode.Gamepad5ButtonY,
            XboxOneKeyCode.Gamepad6ButtonY,
            XboxOneKeyCode.Gamepad7ButtonY,
            XboxOneKeyCode.Gamepad8ButtonY
        };
        
        public static readonly XboxOneKeyCode[] xButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonX,
            XboxOneKeyCode.Gamepad2ButtonX,
            XboxOneKeyCode.Gamepad3ButtonX,
            XboxOneKeyCode.Gamepad4ButtonX,
            XboxOneKeyCode.Gamepad5ButtonX,
            XboxOneKeyCode.Gamepad6ButtonX,
            XboxOneKeyCode.Gamepad7ButtonX,
            XboxOneKeyCode.Gamepad8ButtonX
        };
        
        public static readonly XboxOneKeyCode[] menuButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonMenu,
            XboxOneKeyCode.Gamepad2ButtonMenu,
            XboxOneKeyCode.Gamepad3ButtonMenu,
            XboxOneKeyCode.Gamepad4ButtonMenu,
            XboxOneKeyCode.Gamepad5ButtonMenu,
            XboxOneKeyCode.Gamepad6ButtonMenu,
            XboxOneKeyCode.Gamepad7ButtonMenu,
            XboxOneKeyCode.Gamepad8ButtonMenu
        };
        
        public static readonly XboxOneKeyCode[] viewButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonView,
            XboxOneKeyCode.Gamepad2ButtonView,
            XboxOneKeyCode.Gamepad3ButtonView,
            XboxOneKeyCode.Gamepad4ButtonView,
            XboxOneKeyCode.Gamepad5ButtonView,
            XboxOneKeyCode.Gamepad6ButtonView,
            XboxOneKeyCode.Gamepad7ButtonView,
            XboxOneKeyCode.Gamepad8ButtonView
        };

        public static readonly XboxOneKeyCode[] leftShoulderButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad2ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad3ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad4ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad5ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad6ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad7ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad8ButtonLeftShoulder
        };
        
        public static readonly XboxOneKeyCode[] rightShoulderButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonRightShoulder,
            XboxOneKeyCode.Gamepad2ButtonRightShoulder,
            XboxOneKeyCode.Gamepad3ButtonRightShoulder,
            XboxOneKeyCode.Gamepad4ButtonRightShoulder,
            XboxOneKeyCode.Gamepad5ButtonRightShoulder,
            XboxOneKeyCode.Gamepad6ButtonRightShoulder,
            XboxOneKeyCode.Gamepad7ButtonRightShoulder,
            XboxOneKeyCode.Gamepad8ButtonRightShoulder
        };
        
        public static readonly XboxOneKeyCode[] leftThumbstickButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad2ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad3ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad4ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad5ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad6ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad7ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad8ButtonLeftThumbstick
        };
        
        public static readonly XboxOneKeyCode[] rightThumbstickButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad2ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad3ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad4ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad5ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad6ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad7ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad8ButtonRightThumbstick
        };
        
        public static readonly XboxOneKeyCode[] dPadLeftButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonDPadLeft,
            XboxOneKeyCode.Gamepad2ButtonDPadLeft,
            XboxOneKeyCode.Gamepad3ButtonDPadLeft,
            XboxOneKeyCode.Gamepad4ButtonDPadLeft,
            XboxOneKeyCode.Gamepad5ButtonDPadLeft,
            XboxOneKeyCode.Gamepad6ButtonDPadLeft,
            XboxOneKeyCode.Gamepad7ButtonDPadLeft,
            XboxOneKeyCode.Gamepad8ButtonDPadLeft
        };
        
        public static readonly XboxOneKeyCode[] dPadRightButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonDPadRight,
            XboxOneKeyCode.Gamepad2ButtonDPadRight,
            XboxOneKeyCode.Gamepad3ButtonDPadRight,
            XboxOneKeyCode.Gamepad4ButtonDPadRight,
            XboxOneKeyCode.Gamepad5ButtonDPadRight,
            XboxOneKeyCode.Gamepad6ButtonDPadRight,
            XboxOneKeyCode.Gamepad7ButtonDPadRight,
            XboxOneKeyCode.Gamepad8ButtonDPadRight
        };

        public static readonly XboxOneKeyCode[] dPadUpButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonDPadUp,
            XboxOneKeyCode.Gamepad2ButtonDPadUp,
            XboxOneKeyCode.Gamepad3ButtonDPadUp,
            XboxOneKeyCode.Gamepad4ButtonDPadUp,
            XboxOneKeyCode.Gamepad5ButtonDPadUp,
            XboxOneKeyCode.Gamepad6ButtonDPadUp,
            XboxOneKeyCode.Gamepad7ButtonDPadUp,
            XboxOneKeyCode.Gamepad8ButtonDPadUp
        };
        
        public static readonly XboxOneKeyCode[] dPadDownButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonDPadDown,
            XboxOneKeyCode.Gamepad2ButtonDPadDown,
            XboxOneKeyCode.Gamepad3ButtonDPadDown,
            XboxOneKeyCode.Gamepad4ButtonDPadDown,
            XboxOneKeyCode.Gamepad5ButtonDPadDown,
            XboxOneKeyCode.Gamepad6ButtonDPadDown,
            XboxOneKeyCode.Gamepad7ButtonDPadDown,
            XboxOneKeyCode.Gamepad8ButtonDPadDown
        };
        
        public static readonly XboxOneKeyCode[] anyButtons =
        {
            XboxOneKeyCode.Gamepad1ButtonA,
            XboxOneKeyCode.Gamepad2ButtonA,
            XboxOneKeyCode.Gamepad3ButtonA,
            XboxOneKeyCode.Gamepad4ButtonA,
            XboxOneKeyCode.Gamepad5ButtonA,
            XboxOneKeyCode.Gamepad6ButtonA,
            XboxOneKeyCode.Gamepad7ButtonA,
            XboxOneKeyCode.Gamepad8ButtonA,
            XboxOneKeyCode.Gamepad1ButtonB,
            XboxOneKeyCode.Gamepad2ButtonB,
            XboxOneKeyCode.Gamepad3ButtonB,
            XboxOneKeyCode.Gamepad4ButtonB,
            XboxOneKeyCode.Gamepad5ButtonB,
            XboxOneKeyCode.Gamepad6ButtonB,
            XboxOneKeyCode.Gamepad7ButtonB,
            XboxOneKeyCode.Gamepad8ButtonB,
            XboxOneKeyCode.Gamepad1ButtonY,
            XboxOneKeyCode.Gamepad2ButtonY,
            XboxOneKeyCode.Gamepad3ButtonY,
            XboxOneKeyCode.Gamepad4ButtonY,
            XboxOneKeyCode.Gamepad5ButtonY,
            XboxOneKeyCode.Gamepad6ButtonY,
            XboxOneKeyCode.Gamepad7ButtonY,
            XboxOneKeyCode.Gamepad8ButtonY,
            XboxOneKeyCode.Gamepad1ButtonX,
            XboxOneKeyCode.Gamepad2ButtonX,
            XboxOneKeyCode.Gamepad3ButtonX,
            XboxOneKeyCode.Gamepad4ButtonX,
            XboxOneKeyCode.Gamepad5ButtonX,
            XboxOneKeyCode.Gamepad6ButtonX,
            XboxOneKeyCode.Gamepad7ButtonX,
            XboxOneKeyCode.Gamepad8ButtonX,
            XboxOneKeyCode.Gamepad1ButtonMenu,
            XboxOneKeyCode.Gamepad2ButtonMenu,
            XboxOneKeyCode.Gamepad3ButtonMenu,
            XboxOneKeyCode.Gamepad4ButtonMenu,
            XboxOneKeyCode.Gamepad5ButtonMenu,
            XboxOneKeyCode.Gamepad6ButtonMenu,
            XboxOneKeyCode.Gamepad7ButtonMenu,
            XboxOneKeyCode.Gamepad8ButtonMenu,
            XboxOneKeyCode.Gamepad1ButtonView,
            XboxOneKeyCode.Gamepad2ButtonView,
            XboxOneKeyCode.Gamepad3ButtonView,
            XboxOneKeyCode.Gamepad4ButtonView,
            XboxOneKeyCode.Gamepad5ButtonView,
            XboxOneKeyCode.Gamepad6ButtonView,
            XboxOneKeyCode.Gamepad7ButtonView,
            XboxOneKeyCode.Gamepad8ButtonView,
            XboxOneKeyCode.Gamepad1ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad2ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad3ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad4ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad5ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad6ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad7ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad8ButtonLeftShoulder,
            XboxOneKeyCode.Gamepad1ButtonRightShoulder,
            XboxOneKeyCode.Gamepad2ButtonRightShoulder,
            XboxOneKeyCode.Gamepad3ButtonRightShoulder,
            XboxOneKeyCode.Gamepad4ButtonRightShoulder,
            XboxOneKeyCode.Gamepad5ButtonRightShoulder,
            XboxOneKeyCode.Gamepad6ButtonRightShoulder,
            XboxOneKeyCode.Gamepad7ButtonRightShoulder,
            XboxOneKeyCode.Gamepad8ButtonRightShoulder,
            XboxOneKeyCode.Gamepad1ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad2ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad3ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad4ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad5ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad6ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad7ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad8ButtonLeftThumbstick,
            XboxOneKeyCode.Gamepad1ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad2ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad3ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad4ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad5ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad6ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad7ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad8ButtonRightThumbstick,
            XboxOneKeyCode.Gamepad1ButtonDPadLeft,
            XboxOneKeyCode.Gamepad2ButtonDPadLeft,
            XboxOneKeyCode.Gamepad3ButtonDPadLeft,
            XboxOneKeyCode.Gamepad4ButtonDPadLeft,
            XboxOneKeyCode.Gamepad5ButtonDPadLeft,
            XboxOneKeyCode.Gamepad6ButtonDPadLeft,
            XboxOneKeyCode.Gamepad7ButtonDPadLeft,
            XboxOneKeyCode.Gamepad8ButtonDPadLeft,
            XboxOneKeyCode.Gamepad1ButtonDPadRight,
            XboxOneKeyCode.Gamepad2ButtonDPadRight,
            XboxOneKeyCode.Gamepad3ButtonDPadRight,
            XboxOneKeyCode.Gamepad4ButtonDPadRight,
            XboxOneKeyCode.Gamepad5ButtonDPadRight,
            XboxOneKeyCode.Gamepad6ButtonDPadRight,
            XboxOneKeyCode.Gamepad7ButtonDPadRight,
            XboxOneKeyCode.Gamepad8ButtonDPadRight,
            XboxOneKeyCode.Gamepad1ButtonDPadUp,
            XboxOneKeyCode.Gamepad2ButtonDPadUp,
            XboxOneKeyCode.Gamepad3ButtonDPadUp,
            XboxOneKeyCode.Gamepad4ButtonDPadUp,
            XboxOneKeyCode.Gamepad5ButtonDPadUp,
            XboxOneKeyCode.Gamepad6ButtonDPadUp,
            XboxOneKeyCode.Gamepad7ButtonDPadUp,
            XboxOneKeyCode.Gamepad8ButtonDPadUp,
            XboxOneKeyCode.Gamepad1ButtonDPadDown,
            XboxOneKeyCode.Gamepad2ButtonDPadDown,
            XboxOneKeyCode.Gamepad3ButtonDPadDown,
            XboxOneKeyCode.Gamepad4ButtonDPadDown,
            XboxOneKeyCode.Gamepad5ButtonDPadDown,
            XboxOneKeyCode.Gamepad6ButtonDPadDown,
            XboxOneKeyCode.Gamepad7ButtonDPadDown,
            XboxOneKeyCode.Gamepad8ButtonDPadDown,
        };
        
        public static bool GetButtonAnyDown(out XboxOneKeyCode buttonThatWasPressed)
        {
            for (int i = 0; i < anyButtons.Length; i++)
            {
                XboxOneKeyCode keyCode = anyButtons[i];
                if (XboxOneInput.GetKeyUp(keyCode))
                {
                    buttonThatWasPressed = keyCode;
                    return true;
                }
            }
            buttonThatWasPressed = XboxOneKeyCode.Gamepad1ButtonX;
            return false;
        }
        
        public static bool GetButtonXDown(out XboxOneKeyCode buttonThatWasPressed)
        {
            
    
            for (int i = 0; i < xButtons.Length; i++)
            {
                XboxOneKeyCode keyCode = xButtons[i];
                if (XboxOneInput.GetKeyDown(keyCode))
                {
                    buttonThatWasPressed = keyCode;
                    return true;
                }
            }
            buttonThatWasPressed = XboxOneKeyCode.Gamepad1ButtonX;
            return false;
        }

        public static bool GetButtonADown(out XboxOneKeyCode buttonThatWasPressed)
        {
            for (int i = 0; i < aButtons.Length; i++)
            {
                XboxOneKeyCode keyCode = aButtons[i];
                if (XboxOneInput.GetKeyDown(keyCode))
                {
                    buttonThatWasPressed = keyCode;
                    return true;
                }
            }
            buttonThatWasPressed = XboxOneKeyCode.Gamepad1ButtonA;
            return false;
        }
        
        public static bool GetButtonYDown(out XboxOneKeyCode buttonThatWasPressed)
        {
            for (int i = 0; i < yButtons.Length; i++)
            {
                XboxOneKeyCode keyCode = yButtons[i];
                if (XboxOneInput.GetKeyDown(keyCode))
                {
                    buttonThatWasPressed = keyCode;
                    return true;
                }
            }
            buttonThatWasPressed = XboxOneKeyCode.Gamepad1ButtonY;
            return false;
        }
        

        public static User GetUserWhenPlayerPressButton(uint gamepadIndex)
        {
            int userId = XboxOneInput.GetUserIdForGamepad(gamepadIndex);
            XboxOneInput.RemapGamepadToIndex(gamepadIndex, 1);
            User user = UsersManager.FindUserById(userId);

            Debug.LogFormat("XboxOneInputHelpers. GetUserWhenPlayerPressButton. UserId: {0}, GamepadIndex: {1}", userId, gamepadIndex);

            return user;
        }
}
#endif