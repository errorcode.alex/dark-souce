﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Consoles;
using UnityEngine;

[Serializable]
public class Trophy
{
    public string name { get; private set; }
    public string id { get; private set; }
    public int steps { get; private set; }
    public int currentStep { get; set; }
    public int stepsSend { get; set; }

    public Trophy(string nameTrophy, string idTrophy, int stepsTrophy = 1)
    {
        name = nameTrophy;
        id = idTrophy;
        steps = stepsTrophy;
        currentStep = 0;
        stepsSend = 0;
    }
}
public class TrophyManager
{
    private List<Trophy> trophys = new List<Trophy>();

    private string trophyFileName = "trophy.trp";
    public void Init()
    {
        trophys = new List<Trophy>();
        
        AddAchievement("platinum", "0");
        AddAchievement("Level1", "1"); // ?
        AddAchievement("Level3", "2");// ?
        AddAchievement("Level5", "3"); // ?
        AddAchievement("Level10", "4"); // ?
        AddAchievement("CollectHeart", "5"); // ?
        AddAchievement("Score500", "6");  // ?
        AddAchievement("Score1000", "7"); // ?
        AddAchievement("Score2000", "8"); // ?
        AddAchievement("Score5000", "9"); // ?
        AddAchievement("Die5", "10", 5); // ?
        AddAchievement("Die20", "11", 20); // ?
        AddAchievement("Jump72", "12", 72); // ?
        AddAchievement("Wait60", "13"); // ?
    }

    private static float startTimer = 30f;
    private float timer = startTimer;
    public void Update()
    {
        /*
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = startTimer;
            Save();
        }
        */
    }

    public void Save()
    {
        byte[] bytes;
        IFormatter formatter = new BinaryFormatter();
        using (var stream = new MemoryStream())
        {
            formatter.Serialize(stream, trophys);
            bytes = stream.ToArray();
        }
        
        Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.SaveAsync(trophyFileName, bytes));
    }
    
    public void Load()
    {
        Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.FileExistsAsync(trophyFileName, exists =>
        {
            if (exists)
            {
                Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.ReadAsync(trophyFileName, bytes =>
                {
                    trophys = null;
                    IFormatter formatter = new BinaryFormatter();
                    using (var stream = new MemoryStream(bytes))
                        trophys = (List<Trophy>)formatter.Deserialize(stream);

                    /*
                    foreach (var item in trophys)
                    {
                        Debug.LogFormat("name:{0} id:{1} steps:{2} currentStep:{3} stepsSend:{4}",
                            item.name, item.id, item.steps, item.currentStep, item.stepsSend);
                        
                        if (item.currentStep >= item.steps)
                            RepeatUnlockAchievement(item.name);
                    }
                    */
                }));
            }
        }));
    }
    
    public void ClearAchievementsFile()
    {
        Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.FileExistsAsync(trophyFileName, exists =>
        {
            if (exists)
                Infrastructure.Instance.GoCoroutine(Infrastructure.Instance.Storage.DeleteFileAsync(trophyFileName, Init));
        }));

    }
    
    private void AddAchievement(string nameAchievement, string idAchievement, int steps = 1)
    {
        trophys.Add(new Trophy(nameAchievement, idAchievement, steps));
    }
    
    public bool IsAchievement(string nameAchievement)
    {
        var achievement = trophys.Find(n => n.name == nameAchievement);
        if (achievement == null)
            return false;
        return achievement.currentStep >= achievement.steps;
    }

    public void UnlockAchievement(string nameAchievement)
    {
        var achievement = trophys.Find(n => n.name == nameAchievement);

        if (achievement == null)
            return;
        if (achievement.currentStep >= achievement.steps)
            return;

        achievement.currentStep++;

        var proc = achievement.currentStep * 100 / achievement.steps;
        var id = achievement.id;

//        Debug.LogFormat("<color=blue>Unlocking achievement {0}, currentStep : {1}, steps : {2}</color>", nameAchievement, achievement.currentStep, achievement.steps);

        if (achievement.currentStep >= achievement.steps)
        {
//            Debug.LogFormat("<color=green>Unlocking achievement {0}</color>", nameAchievement);
#if (UNITY_SWITCH || UNITY_PS4 || UNITY_XBOXONE) && !UNITY_EDITOR
            Infrastructure.Instance.Manager.UnlockingAchievement(id);
#endif
        }
    }

    public void RepeatUnlockAchievement(string nameAchievement)
    {
        var achievement = trophys.Find(n => n.name == nameAchievement);

        if (achievement == null)
            return;
        var id = achievement.id;
        Debug.LogFormat("<color=green>Unlocking achievement {0}</color>", nameAchievement);
#if (UNITY_SWITCH || UNITY_PS4 || UNITY_XBOXONE) && !UNITY_EDITOR
        Infrastructure.Instance.Manager.UnlockingAchievement(id);
#endif
    }
}
