using System;
using System.Collections;
using System.IO;
using System.Text;

using UnityEngine;


public class StandaloneLocalStorage : IStorage
{
    private string _applicationPath;
    private const float ThreadMillisecondsTimeout = 0.01f;

    private int _countSaves;
    private double _lastInterval;
    private float _savesPerMinutes;
    private const float LimitsFrequencyWriting = 32f;
    private const float UpdateInterval = 60f;

    public void Update()
    {
    }
    
    private string GetFullPath(string fileName)
    {
        Debug.LogError(fileName);
        if(string.IsNullOrEmpty(_applicationPath))
            throw new NullReferenceException("_applicationPath ������ ���� ����������������� ������ ��� ��������������");

        return Path.Combine(_applicationPath, fileName);
    }

    public void Init()
    {
        _applicationPath = Application.persistentDataPath;
        _lastInterval = Time.realtimeSinceStartup;
    }

    public IEnumerator SaveAsync(string fileName, byte[] content, Action onComplete = null)
    {
        Save(fileName, content);
        if(onComplete != null)
            onComplete.Invoke();
        yield return null;
    }

    public IEnumerator SaveAsync(string fileName, string content, Action onComplete = null)
    {
        yield return new WaitForSeconds(ThreadMillisecondsTimeout);
        var bytes = Encoding.UTF8.GetBytes(content);
        yield return SaveAsync(fileName, bytes, onComplete);
    }

    public IEnumerator ReadAsync(string fileName, Action<byte[]> onComplete)
    {
        yield return new WaitForSeconds(ThreadMillisecondsTimeout);
        if(onComplete != null)
            onComplete.Invoke(Read(fileName));
        
        yield return null;
    }

    public IEnumerator ReadTextAsync(string fileName, Action<string> onComplete)
    {
        yield return new WaitForSeconds(ThreadMillisecondsTimeout);
        if(onComplete != null)
            onComplete.Invoke(ReadText(fileName));
        
        yield return null;
    }

    public IEnumerator FileExistsAsync(string fileName, Action<bool> onExists)
    {
        yield return new WaitForSeconds(ThreadMillisecondsTimeout);
        if(onExists != null)
            onExists.Invoke(FileExists(fileName));
        
        yield return null;
    }

    public IEnumerator DeleteFileAsync(string fileName, Action onComplete = null)
    {
        yield return new WaitForSeconds(ThreadMillisecondsTimeout);
        DeleteFile(fileName);
        if(onComplete != null)
            onComplete.Invoke();
        yield return null;
    }

    public void Save(string fileName, byte[] content)
    {
        ++_countSaves;
        var timeNow = Time.realtimeSinceStartup;
        if (timeNow > _lastInterval + UpdateInterval)
        {
            _savesPerMinutes = _countSaves / ((timeNow - (float)_lastInterval) / 60);
            _countSaves = 0;
            _lastInterval = timeNow;
            
            Debug.LogWarningFormat("<color={0}>frequency of writing to storage media {1} saves per minutes </color>",
                _savesPerMinutes >= LimitsFrequencyWriting ? "red" : "green", _savesPerMinutes);
        }
        
        var fullPath = GetFullPath(fileName);
        var e = new System.Threading.AutoResetEvent(false);

        using (var stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write, FileShare.Read))
        {
            stream.BeginWrite(content, 0, content.Length, asyncResult => { e.Set(); }, e);

            e.WaitOne();
        }
//        Debug.LogError(fullPath);
    }

    public void Save(string fileName, string content)
    {
        var bytes = Encoding.UTF8.GetBytes(content);

        Save(fileName, bytes);
    }

    public byte[] Read(string fileName)
    {
        return File.ReadAllBytes(GetFullPath(fileName));
    }

    public string ReadText(string fileName)
    {
        return File.ReadAllText(GetFullPath(fileName), Encoding.UTF8);
    }

    public bool FileExists(string fileName)
    {
        return File.Exists(GetFullPath(fileName));
    }

    public void DeleteFile(string fileName)
    {
        File.Delete(GetFullPath(fileName));
    }

    public void Cleanup()
    {
    }
}
