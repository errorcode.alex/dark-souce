﻿using System.Collections;
using System.Collections.Generic;
using Consoles;
using UnityEngine;

public class OnScreenLogInGame : Singleton<OnScreenLogInGame>
{
    int msgCount = 0;
    private List<string> log = new List<string>();
    int maxLines = 16;
    int fontSize = 24;
    int frameCount = 0;

    void Update ()
    {
        frameCount ++;
    }

    public virtual void OnGUI()
    {
#if SCREEN_LOG_IN_GAME
        GUIStyle style = GUI.skin.GetStyle("Label");
        style.fontSize = fontSize;
        style.alignment = TextAnchor.UpperLeft;
        style.wordWrap = false;

        float height = 0;
        string logText = "";
        foreach(string s in log)
        {
            logText += " " + s;
            logText += "\n";
            height += style.lineHeight;
        }
        height += 6;

        GUI.Label(new Rect(0, 0, Screen.width - 1, height), logText, style);

        height = style.lineHeight + 4;
        GUI.Label(new Rect(Screen.width - 100, Screen.height - 100, Screen.width - 1, height), frameCount.ToString());
#endif
    }

    public void Add(string msg)
    {
        string cleaned = msg.Replace("\r", " ");
        cleaned = cleaned.Replace("\n", " ");

        System.Console.WriteLine("[DarkestvilleCastle] " + cleaned);
        Debug.Log("[DarkestvilleCastle] " + cleaned);

        log.Add(cleaned);
        msgCount ++;

        if(msgCount > maxLines)
        {
            log.RemoveAt(0);
        }
    }
}
