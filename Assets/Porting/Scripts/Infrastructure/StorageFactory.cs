using UnityEngine;

public static class StorageFactory
{
    public static IStorage Create()
    {
#if UNITY_SWITCH && !UNITY_EDITOR
        return new NintendoSwitchStorage();
#elif UNITY_PS4 && !UNITY_EDITOR
        return new PS4Storage();
#elif UNITY_XBOXONE && !UNITY_EDITOR
        return new XBoxOneStorage();
#elif STEAM_BUILD && !UNITY_EDITOR
        return new StandaloneLocalStorage();
#else
        return new StandaloneLocalStorage();
        //return new PS4StorageStanalone();
#endif
    }
}
