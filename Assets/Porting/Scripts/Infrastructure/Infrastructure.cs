using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Consoles
{
    public class Infrastructure : Singleton<Infrastructure>
    {
        public BaseManager Manager { get; private set; }
        public IStorage Storage { get; private set; }

        public TrophyManager TrophyManager { get; private set; }
        
        public XboxSignInUser XboxSignInUser { get; private set; }
        void OnEnable() 
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnDisable() 
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode) 
        {
#if UNITY_SWITCH || UNITY_XBOXONE
            //PlayerPrefs.SaveStateChanges();
#endif
#if !UNITY_SWITCH
            Instance.TrophyManager.Save();
#endif
        }
        
        public void Init()
        {
            Instance.Manager = ManagerFactory.Create(gameObject);
            Instance.Manager.Init();
            
            Instance.Storage = StorageFactory.Create();
            Instance.Storage.Init();
            
            Instance.TrophyManager = new TrophyManager();
            Instance.TrophyManager.Init();

            Instance.XboxSignInUser = gameObject.AddComponent<XboxSignInUser>();
            /*
            s_Common.OnRuntimeMethodLoad();
            s_SaveData.OnRuntimeMethodLoad();
            s_TextData.OnRuntimeMethodLoad();
            s_Player.OnRuntimeMethodLoad();
            */
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            //TrophyManager.Cleanup();
            Manager.Cleanup();
            Storage.Cleanup();
        }

        private float timer = 30;
        private float time = 30;

        public void Update()
        {
            Instance.Storage.Update();
            Instance.TrophyManager.Update();
#if UNITY_SWITCH || UNITY_XBOXONE
            time -= Time.deltaTime;
            if (time <= 0)
            {
          //      PlayerPrefs.SaveStateChanges();
                time = timer;
            }
#endif
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void OnRuntimeMethodLoad()
        {
//#if UNITY_XBOXONE && !UNITY_EDITOR
//            Instance.StartCoroutine(UnityEngine.XboxOne.PlayerPrefs.InitializeAsync(Instance.Init));
//#else
            Instance.Init();
//#endif
        }
        
        public void GoCoroutine(IEnumerator saveCoroutine)
        {
            StartCoroutine(saveCoroutine);
        }
    }
}