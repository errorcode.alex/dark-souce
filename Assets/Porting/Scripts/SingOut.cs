﻿using System.Collections;
using System.Collections.Generic;
using Consoles;
using UnityEngine;

public class SingOut : MonoBehaviour
{
    void OnEnable() 
    {
        Infrastructure.Instance.Manager.onUserSignOut += OnUserSignOut;
    }

    void OnDisable() 
    {
        Infrastructure.Instance.Manager.onUserSignOut -= OnUserSignOut;
    }

    private void OnUserSignOut()
    {
        MainMenu.loadStartMenu = false;
        Application.LoadLevel("MainMenu");
    }
}
