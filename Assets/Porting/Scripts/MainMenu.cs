﻿using System;
using System.Collections;
using Consoles;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using Rewired;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject continueButton;
    
    public MMAchievementRules achievementRules;
    public GameObject prassAnyButton;
    public GameObject userPanel;
    public GameObject menu;
    public Text userName;
    public Transform startCamera;
    public Transform toCamera;
    public Transform cameraTransform;
    public float camMoveSpeed;
    private Player player;

    public GameObject panelMainMenu;
    public GameObject panelLevelSelect;
    public GameObject panelAYouSure;
    
    public static bool firstLoad;
    public static bool loadStartMenu;
    [Serializable]
    public enum StateMenu
    {
        pressAnyButton,
        userPanel,
        other
    }

    public StateMenu stateMenu = StateMenu.pressAnyButton;

    private static readonly string ButtonUp = "ButtonUp";
    private static readonly string ButtonDown = "ButtonDown";
    private static readonly string ButtonLeft = "ButtonLeft";
    private static readonly string ButtonRight = "ButtonRight";
    private static readonly string ArrowLeft = "ArrowLeft";
    private static readonly string ArrowUp = "ArrowUp";
    private static readonly string ArrowDown = "ArrowDown";
    private static readonly string ArrowRight = "ArrowRight";
    
    private readonly string[] actionsButtons = {ButtonUp, ButtonDown, ButtonLeft, ButtonRight, ArrowLeft, ArrowUp, ArrowDown, ArrowRight};
    private readonly string[] cheatsButtons = {ArrowUp, ArrowUp, ArrowDown, ArrowDown, ArrowUp, ArrowUp, ArrowRight, ArrowLeft, ArrowLeft, ArrowRight, ButtonLeft};
    //private readonly string[] cheatsButtons = {ArrowUp, ArrowUp, ArrowDown, ArrowDown};
    
    private int cheatsIndex;

    public static MainMenu Instance;
    public void SetStatusOther()
    {
        stateMenu = StateMenu.other;
    }
    
    public void SetStatusUserPanel()
    {
        stateMenu = StateMenu.userPanel;
    }
    
    
    private void Awake()
    {
        Instance = this;
        player = ReInput.players.GetPlayer(0);
        prassAnyButton.SetActive(true);
        menu.SetActive(false);
        userPanel.SetActive(false);
        cameraTransform.position = startCamera.position;
    }
    
    void Start ()
    {
#if UNITY_EDITOR
        firstLoad = false;
#endif
        firstLoad = false;
        if(firstLoad)
            Invoke("change_firstLoad", 9);
        
        if(loadStartMenu)
            StartMenu();
        loadStartMenu = true;
    }
    private void StartMenu()
    {
        RetroAdventureProgressManager.Instance.LoadService();
        stateMenu = StateMenu.userPanel;

        if (RetroAdventureProgressManager.Instance.IsNewGame())
            continueButton.SetActive(false);
        else
            continueButton.SetActive(true);
        
        prassAnyButton.SetActive(false);
        menu.SetActive(true);
        
#if UNITY_XBOXONE_UI
        userPanel.SetActive(true);
#endif
        cameraTransform.position = toCamera.position;
    }
    void change_firstLoad()
    {
        firstLoad = false;
    }
    private void OnEnable()
    {
        Infrastructure.Instance.XboxSignInUser.OnSignIn += OnSignIn;
        Infrastructure.Instance.XboxSignInUser.OnReloadProfile += OnReloadProfile;
    }

    private void OnDisable()
    {
        Infrastructure.Instance.XboxSignInUser.OnSignIn -= OnSignIn;
        Infrastructure.Instance.XboxSignInUser.OnReloadProfile += OnReloadProfile;
    }
    
    private void OnSignIn(bool anyButtonType)
    {
        if (anyButtonType)
        {
            //anyButton = true;
        }
        else
        {
        
        }
    
        Reload();
    }
    private void OnReloadProfile(bool anyButtonType)
    {
        if (anyButtonType)
        {

        }
        else
        {
            Reload();
        }
    }

    private void Reload()
    {
#if UNITY_XBOXONE
        PlayerPrefs.ReLoad(ReInit);
#else
		ReInit();
#endif
    }

    private void ReInit()
    {
#if UNITY_XBOXONE
        userName.text = XBoxOneStorageUtils.GameDisplayName;
#endif
        
        if (!PlayerPrefs.HasKey("UserLanguage"))
            PlayerPrefs.SetString("UserLanguage", SystemLanguageString);
        SimpeTranslate.UpdateLanguage();
        Infrastructure.Instance.TrophyManager.Load();
        SoundManager.Instance.LoadService();
        RetroAdventureProgressManager.Instance.LoadService();
        GameManager.Instance.Load();
        achievementRules.LoadService();
        stateMenu = StateMenu.userPanel;

        if (RetroAdventureProgressManager.Instance.IsNewGame())
            continueButton.SetActive(false);
        else
            continueButton.SetActive(true);
        
        prassAnyButton.SetActive(false);
        //cameraTransform.position = toCamera.position;
        StartCoroutine(TranslateCamera(() =>
        {
            
            menu.SetActive(true);
        
#if UNITY_XBOXONE_UI
            userPanel.SetActive(true);
#endif
        },
        () =>
        {
            
        }));
        

    }

    IEnumerator TranslateCamera(Action onComplete1, Action onComplete2)
    {
        while(Vector3.Distance(cameraTransform.position, toCamera.position) > 0.3f)
        {

            cameraTransform.position = Vector3.Lerp(cameraTransform.position, toCamera.position, camMoveSpeed);
            yield return new WaitForSeconds(0.001f);
        }
        onComplete1.Invoke();
        while(Vector3.Distance(cameraTransform.position, toCamera.position) > 0.01f)
        {

            cameraTransform.position = Vector3.Lerp(cameraTransform.position, toCamera.position, camMoveSpeed);
            yield return new WaitForSeconds(0.001f);
        }
        cameraTransform.position = toCamera.position;
        onComplete2.Invoke();
        
    }
    

    void Update ()
    {
        if(firstLoad)
            return;
        
        if (stateMenu == StateMenu.pressAnyButton)
        {
#if UNITY_XBOXONE && !UNITY_EDITOR
            Infrastructure.Instance.XboxSignInUser.UpdateAnyButton();
#else
            if (player.GetAnyButtonDown())
                OnSignIn(true);
#endif
        }

        if (stateMenu == StateMenu.userPanel)// && faceBookUINode.gameObject.activeSelf)
        {
#if UNITY_XBOXONE && !UNITY_EDITOR
            Infrastructure.Instance.XboxSignInUser.UpdateChangeUser();
#endif
#if UNITY_XBOXONE && UNITY_EDITOR
            
            if (Input.GetButtonDown ("Player1_Dash"))
            {
                Debug.LogError("GamepadY");
                Reload();
            }
#endif
            
#if UNITY_XBOXONE && UNITY_EDITOR
            
            if (Input.GetButtonDown ("Player1_Jetpack"))
            {
                Debug.LogError("GamepadX");
                ((XboxOneManager)Infrastructure.Instance.Manager).TestOnUserSignOut();
            }
#endif

            foreach (var button in actionsButtons)
            {
                
                if (!player.GetButtonDown(button))
                    continue;
                //Debug.LogError("GetButtonDown : " + button);
                if (button == cheatsButtons[cheatsIndex])
                {
                    if (++cheatsIndex == cheatsButtons.Length)
                    {
                        Debug.LogError(button);
                        cheatsIndex = 0;
                        OpenCheats();
                    }
                    break;
                }
                cheatsIndex = 0;
            }
        }
    }
    
    public void OpenCheats()
    {
        Debug.LogError("OpenCheats");
        RetroAdventureProgressManager.Instance.OpenAllLevel();
        continueButton.SetActive(true);
    }
    
    public void ResetProgress()
    {
        
        RetroAdventureProgressManager.Instance.ResetProgress ();
        continueButton.SetActive(false);
    }

    public void NewGame()
    {
        if (RetroAdventureProgressManager.Instance.IsNewGame())
        {
            panelMainMenu.SetActive(false);
            panelLevelSelect.SetActive(true);
        }
        else
        {
            panelMainMenu.SetActive(false);
            panelAYouSure.SetActive(true);
        }
    }
    
    //With absolute value
    public bool ApproximatelyAbsolute(Vector3 me, Vector3 other, float allowedDifference)
    {
        var dx = me.x - other.x;
        if (Mathf.Abs(dx) > allowedDifference)
            return false;
 
        var dy = me.y - other.y;
        if (Mathf.Abs(dy) > allowedDifference)
            return false;
 
        var dz = me.z - other.z;
 
        return Mathf.Abs(dz) >= allowedDifference;
    }
    //With percentage i.e. between 0 and 1
    public bool ApproximatelyPercentage(Vector3 me, Vector3 other, float percentage)
    {
        var dx = me.x - other.x;
        if (Mathf.Abs(dx) > me.x * percentage)
            return false;
 
        var dy = me.y - other.y;
        if (Mathf.Abs(dy) > me.y * percentage)
            return false;
 
        var dz = me.z - other.z;
 
        return Mathf.Abs(dz) >= me.z * percentage;
    }
    
    public static string SystemLanguageString
    {
        get
        {
            return "eng";
#if UNITY_SWITCH
            var currentLanguage = nn.oe.Language.GetDesired();
            switch (currentLanguage)
            {
                case "en-Us":
                case "en-GB":
                    return "eng";
                case "ru":
                    return "rus";
                default:
                    return "eng";
            }
#elif UNITY_PS4 && !UNITY_EDITOR
            var currentLanguage = (int)UnityEngine.PS4.Utility.systemLanguage;
            switch (currentLanguage)
            {
                case 1:         // English (United States)
                case 18:        // English (United Kingdom)
                    return "eng";
                case 8:        // Russian
                    return "rus";
                default:
                    return "eng";
            }
#else
            
            switch (Application.systemLanguage)
            {
                case SystemLanguage.English:
                    return "eng";
                case SystemLanguage.Russian:
                    return "rus";
                default:
                    return "eng";
            }
#endif
        }
    }

    public void Test()
    {
        LevelManager.Instance.GotoLevel("Level 20");
    }
}
