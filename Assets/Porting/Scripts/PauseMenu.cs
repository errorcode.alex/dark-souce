﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.CorgiEngine;
using Rewired;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private static readonly string ButtonUp = "ButtonUp";
    private static readonly string ButtonDown = "ButtonDown";
    private static readonly string ButtonLeft = "ButtonLeft";
    private static readonly string ButtonRight = "ButtonRight";
    private static readonly string ArrowLeft = "ArrowLeft";
    private static readonly string ArrowUp = "ArrowUp";
    private static readonly string ArrowDown = "ArrowDown";
    private static readonly string ArrowRight = "ArrowRight";
    
    private readonly string[] actionsButtons = {ButtonUp, ButtonDown, ButtonLeft, ButtonRight, ArrowLeft, ArrowUp, ArrowDown, ArrowRight};
    private readonly string[] cheatsButtons = {ArrowUp, ArrowUp, ArrowDown, ArrowDown, ArrowUp, ArrowUp, ArrowRight, ArrowLeft, ArrowLeft, ArrowRight, ButtonLeft};
    //private readonly string[] cheatsButtons = {ArrowUp, ArrowUp, ArrowDown, ArrowDown};
    
    private int cheatsIndex;

    private Player player;
    
    void Start()
    {
        player = ReInput.players.GetPlayer(0);
    }

    void Update()
    {
        
        foreach (var button in actionsButtons)
        {
                
            if (!player.GetButtonDown(button))
                continue;
            //Debug.LogError("GetButtonDown : " + button);
            if (button == cheatsButtons[cheatsIndex])
            {
                if (++cheatsIndex == cheatsButtons.Length)
                {
                    Debug.LogError(button);
                    cheatsIndex = 0;
                    OpenCheats();
                }
                break;
            }
            cheatsIndex = 0;
        }
    }
    
    public void OpenCheats()
    {
        Debug.LogError("OpenCheats");
        var health = GameObject.Find("Pig Hero").GetComponent<Health>();
        health.ResetHealthToMaxHealth();
    }
}
