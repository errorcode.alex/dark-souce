﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class GlobalListnerInput : MonoBehaviour
{
    [SerializeField] private PairButton _pairButton;
    
    [SerializeField] private List<string> onlyInWindows;

    private Player player;
    private bool canUpdate = true;
    void Start()
    {
        player = ReInput.players.GetPlayer(0);
    }

    void Update()
    {
        if (onlyInWindows.Count != 0)
        {
            canUpdate = false;
            foreach (var onlyInWindow in onlyInWindows)
            {
                //Debug.LogError("onlyInWindows:"+InputController.managersStack.Peek());
                if (InputController.managersStack.Peek().name == onlyInWindow)
                {
                    canUpdate = true;
                }
            }
        }

        if (!canUpdate)
            return;
        switch (_pairButton.Button)
        {
            
                
            case BUTTONS_NAME.SUBMIT:
            {
                if (player.GetButtonDown("UISubmit") && _pairButton.UIButton.gameObject.activeSelf)
                    _pairButton.UIButton.onClick.Invoke();

                break;
            }
            case BUTTONS_NAME.CANCEL:
            {
                if (player.GetButtonDown("UICancel") && _pairButton.UIButton.gameObject.activeSelf)
                    _pairButton.UIButton.onClick.Invoke();
                    
                break;
            }
                /*
            case BUTTONS_NAME.UP:
            {
                if (Input.GetButtonDown("UIUp") && _pairButton.UIButton.gameObject.activeSelf)
                    _pairButton.UIButton.onClick.Invoke();
                    
                break;
            }
            case BUTTONS_NAME.LEFT:
            {
                if (Input.GetButtonDown("UILeft") && _pairButton.UIButton.gameObject.activeSelf)
                    _pairButton.UIButton.onClick.Invoke();
                break;
            }
            case BUTTONS_NAME.OPTIONS:
            {
                if (Input.GetButtonDown("UIOptions") && _pairButton.UIButton.gameObject.activeSelf)
                    _pairButton.UIButton.onClick.Invoke();
                break;
            }
            */
        }
    }
}
