﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;
public class ItsAlmostAStack 
{
    private List<InputController> items = new List<InputController>();

    public List<InputController> list
    {
        get { return items; }
        private set
        {}
    }
    public void Push(InputController item)
    {
        if (items.Count != 0)
        {
            if (item.sort <= items[0].sort && item.sort != 0)
            {
                items.Insert(0, item);
                return;
            }
        }
        else
        {
            items.Add(item);
            return;
        }

        // [0] = -5; [1] = 0; [2] = 0; [3] = 1
        for (var n = items.Count - 1; n > 0 ; n--)
        {
            if (item.sort > items[n].sort && item.sort < items[n - 1].sort)
            {
                items.Insert(n - 1, item);
                return;
            }
        }
        
        if (item.sort >= items[items.Count - 1].sort)
            items.Add(item);
    }
    public InputController Pop(InputController current)
    {
        for (var n = 0; n < items.Count; n++)
        {
            if (items[n] == current)
            {
//                Debug.LogError("remove " + current.name);
                items.RemoveAt(n);
                return current;
            }
        }
        return default(InputController); 
        if (items.Count > 0)
        {
            InputController temp = items[items.Count - 1];
            items.RemoveAt(items.Count - 1);
            return temp;
        }
        else
            return default(InputController);
    }
    public void Remove(int itemAtPosition)
    {
        items.RemoveAt(itemAtPosition);
    }

    public InputController Peek()
    {
        if (items.Count > 0)
        {
            /*
            for (var n = items.Count - 1; n >= 0; n--)
                if (items[n].Priority)
                    return items[n];
            */
            /*
            for (var n = items.Count - 1; n >= 0; n--)
                if (items[n].NonPriority)
                    return items[n];
            */

            InputController temp = items[items.Count - 1];
            return temp;
        }
        else
            return default(InputController);
    }
}

enum BUTTONS_NAME
{
    UP,
    SUBMIT,
    LEFT,
    CANCEL,
    OPTIONS,
    RIGHT,
    DOWN
}

[Serializable]
struct PairButton
{
    public Button UIButton;
    public BUTTONS_NAME Button;
}
public class InputController : MonoBehaviour
{
    
    [SerializeField] private List<PairButton> _pairButtons;
    [FormerlySerializedAs("FerstSelectButton")]
    [SerializeField]
    private GameObject FirstSelectButton;
    [SerializeField]
    private bool OnlyControllerButtons;
    public bool Priority;
    public bool NonPriority;
    public int sort = 0;
    public static readonly ItsAlmostAStack managersStack = new ItsAlmostAStack();

    private GameObject LastSelectButton = null;
    private void Start()
    {
        gameObject.tag = "UIRaycast";
    }
    public void OnEnable()
    {
        Invoke("Enable", 0.1f);

//        if (Camera.main != null)
//        {
//            Vector2 center = new Vector2(Screen.width / 2, Screen.height / 2);
//            Ray _ray = Camera.main.ScreenPointToRay(center);
//            RaycastHit[] hits = new RaycastHit[20];
//            Physics.RaycastNonAlloc(_ray, hits);
//            for (int i = 0; i < hits.Length; i++)
//            {
//                RaycastHit hit = hits[i];
//                if (hits[i].collider != null)
//                {
//                    Debug.Log("hit : " + hit.collider.gameObject.name);
//                }
//            }
//        }

        /*
        Ray _ray = Camera.current.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.RaycastAll(_ray, out hit)) {
            if (hit.collider.gameObject.CompareTag("UIRaycast")) {
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    Debug.Log("Complete!");
            }
        }
        */
    }

    void Enable()
    {
        if (!NonPriority)
            managersStack.Push(this);
        
        EventSystem.current.SetSelectedGameObject(null);
    }
    public void OnDisable()
    {
        //if (managersStack.Peek() == this)
        {
//            Debug.LogError("Pop " +this.name);

            Invoke("Disable", 0.1f);

        }
    }

    void Disable()
    {
        EventSystem.current.SetSelectedGameObject(null);
        managersStack.Pop(this);
    }
    void OnDestroy()
    {
        for (var n = 0; n < managersStack.list.Count; n++)
        {
            if (managersStack.list[n] == this)
            {
//                Debug.LogError("remove " +this.name);
                EventSystem.current.SetSelectedGameObject(null);
                managersStack.list.RemoveAt(n);
                
                return;
            }
        }
    }

    public bool IsActiveThis()
    {
        if (NonPriority && managersStack.list.Count == 0)
            return true;
//        Debug.LogError("peek " + managersStack.Peek().name);
        return managersStack.Peek() == this;
    }
    private void Update()
    {
        if (!IsActiveThis())
            return;

        foreach (var pairButton in _pairButtons)
        {
            switch (pairButton.Button)
            {
                case BUTTONS_NAME.SUBMIT:
                {
                    if (Input.GetButtonDown("UISubmit") && pairButton.UIButton.gameObject.activeSelf)
                        if (pairButton.UIButton.gameObject.activeSelf)
                            pairButton.UIButton.onClick.Invoke();

                    break;
                }
                case BUTTONS_NAME.CANCEL:
                {
                    if (Input.GetButtonDown("UICancel") && pairButton.UIButton.gameObject.activeSelf)
                        if (pairButton.UIButton.gameObject.activeSelf)
                            pairButton.UIButton.onClick.Invoke();
                    
                    break;
                }
                case BUTTONS_NAME.UP:
                {
                    if (Input.GetButtonDown("UIUp") && pairButton.UIButton.gameObject.activeSelf)
                        if (pairButton.UIButton.gameObject.activeSelf)
                            pairButton.UIButton.onClick.Invoke();
                    
                    break;
                }
                case BUTTONS_NAME.LEFT:
                {
                    if (Input.GetButtonDown("UILeft") && pairButton.UIButton.gameObject.activeSelf)
                        if (pairButton.UIButton.gameObject.activeSelf)
                            pairButton.UIButton.onClick.Invoke();
                    
                    break;
                }
                case BUTTONS_NAME.RIGHT:
                {
                    if (Input.GetButtonDown("UIRight") && pairButton.UIButton.gameObject.activeSelf)
                        if (pairButton.UIButton.gameObject.activeSelf)
                            pairButton.UIButton.onClick.Invoke();
                    
                    break;
                }
                case BUTTONS_NAME.DOWN:
                {
                    if (Input.GetButtonDown("UIDown") && pairButton.UIButton.gameObject.activeSelf)
                        if (pairButton.UIButton.gameObject.activeSelf)
                            pairButton.UIButton.onClick.Invoke();
                    
                    break;
                }
            }
        }
/*
        if (FirstSelectButton == null && !OnlyControllerButtons)
        {
            EventSystem.current.SetSelectedGameObject(null);
            return;
        }
*/
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            if (FirstSelectButton != null)
            {
                var CurrontSelectButton = LastSelectButton == null ? FirstSelectButton : LastSelectButton;
                //Debug.LogError("FirstSelectButton = null " + name +"    "+managersStack.Peek().name);
                if (IsActiveThis())
                    EventSystem.current.SetSelectedGameObject(CurrontSelectButton);
            }
        }

        if (EventSystem.current.currentSelectedGameObject != null)
            LastSelectButton = EventSystem.current.currentSelectedGameObject;
    }

}
