﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlatformDifference : MonoBehaviour
{
    public GameObject XboxOne;
    public GameObject Switch;
    public GameObject PS4;
    void Start()
    {
#if UNITY_SWITCH_UI
        XboxOne.SetActive(false);
        Switch.SetActive(true);
        PS4.SetActive(false);
#elif UNITY_PS4_UI
        XboxOne.SetActive(false);
        Switch.SetActive(false);
        PS4.SetActive(true);
#elif UNITY_XBOXONE_UI
        XboxOne.SetActive(true);
        Switch.SetActive(false);
        PS4.SetActive(false);
#endif
    }
}
