﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class ShootCanon : MonoBehaviour
    {

        public GameObject Projectile;
        public Transform Spawner;
        public GameObject MuzzleFlash;

    public float MaxTime;
    public float CurTime;

    public Animator _anim;

    public bool CanShoot = true;

    private void Start()
    {
        _anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (CanShoot == false)
        {
            CurTime -= 1 * Time.deltaTime;
        }

        if (CurTime <= 0)
        {
            CanShoot = true;
            CurTime = MaxTime;
        }
    }   

        public void ShootThis()
        {
        if (CanShoot == true)
            {
            _anim.SetTrigger("Fire");
                Instantiate(Projectile, Spawner.position, Spawner.rotation);
                Instantiate(MuzzleFlash, Spawner.position, Spawner.rotation);
                CanShoot = false;
            }
        }
    }