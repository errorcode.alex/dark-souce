﻿using System.Collections;
using UnityEngine;
#if !DISABLESTEAMWORKS
using Steamworks;
#endif
public class AchievementSteam : MonoBehaviour
{

    public string AchievementID = "ach_a";
    public bool unlocked, Triggered;
    // Use this for initialization


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (Triggered)
            {
                Unlock();
            }

        }
    }

    public void Unlock()
    {
#if !DISABLESTEAMWORKS
        SteamUserStats.GetAchievement(AchievementID, out unlocked);
        if (!unlocked)
        {
            //(SteamAchievements.script.gameObject.activeSelf)
            //SteamAchievements.script.UnlockSteamAchievement (AchievementID);
            Debug.Log("Unlocked: " + AchievementID);
            SteamUserStats.SetAchievement(AchievementID);

            //
        }
#endif
    }
}