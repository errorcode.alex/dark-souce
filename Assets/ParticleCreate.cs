﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCreate : MonoBehaviour {

    public GameObject system;
	// Update is called once per frame
	void PlayParticleSystem () {
        system.GetComponent<ParticleSystem>().Play();
    }
}
