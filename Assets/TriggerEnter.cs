﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
    public class TriggerEnter : MonoBehaviour
    {
        // Аниматоры
        public Animator anim;
        public Animator TargetAnim; // Аниматор цели на 

        public string RunTag; // Тег необходимый объекту для срабатывания
        public bool PlayerTag; // Срабатывает ли при входе в зону игрока

        // Одноразовое использование
        public MovingPlatform _movingPlatform;


        void Start()
        {
            anim = anim.GetComponent<Animator>();

            if (TargetAnim != null)
            { TargetAnim = TargetAnim.GetComponent<Animator>();}

        }

        // Update is called once per frame

        void OnTriggerEnter2D(Collider2D other)
        {
            // 
            if (PlayerTag)
            {
                if ((other.tag == RunTag) || (other.tag == "Player"))
                {
                    anim.SetBool("Run", true); // Animation Run

                    if (_movingPlatform != null)
                    { _movingPlatform.AuthorizeMovement(); }

                    if (TargetAnim != null)
                    { TargetAnim.SetBool("Run", true); }
                }
            }

            else

           if (other.tag == RunTag)
            {
                anim.SetBool("Run", true);

             if (_movingPlatform != null)
                { _movingPlatform.AuthorizeMovement(); }

             if (TargetAnim != null)
                { TargetAnim.SetBool("Run", true); }

            }

        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (PlayerTag)
            {
                if ((other.tag == RunTag) || (other.tag == "Player"))
                {
                    anim.SetBool("Run", false);

                    if (TargetAnim != null)
                    { TargetAnim.SetBool("Run", false); }
                }
            }

            else

             if (other.tag == RunTag)
            {
                anim.SetBool("Run", false);

                if (TargetAnim != null)
                { TargetAnim.SetBool("Run", false); }
            }
        }
    }
}
