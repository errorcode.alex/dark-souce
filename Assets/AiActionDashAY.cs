﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public class AiActionDashAY : AIAction
    {

        public CharacterDash _characterDash;

        // Use this for initialization
        protected override void Initialization()
        {
            _characterDash = this.gameObject.GetComponent<CharacterDash>();
        }

        // Update is called once per frame
        public override void PerformAction()
        {
            _characterDash.InitiateDash();
        }
    }
}