﻿using UnityEngine;
using System.Collections;
using Consoles;
using MoreMountains.Tools;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace MoreMountains.CorgiEngine
{	
	/// <summary>
	/// This class handles the GUI in the action phases of the Retro Adventure levels
	/// </summary>
	public class RetroAdventureGUIManager : GUIManager, MMEventListener<CorgiEngineEvent>
	{
        [Header("Artalasky Edit")]
        public GameObject LevelFailedSplash;

        [Header("RetroAdventure")]
		/// the text used to display collected stars number
		public Text StarDisplayText; 
		/// the splash to display when the level is complete
		public GameObject LevelCompleteSplash;
		/// the object to give focus to when the complete splash gets displayed
		public GameObject LevelCompleteSplashFocus;
		/// the GUI inventory displays
		public GameObject Inventories;
		/// the representation of the collected stars
		public Image[] Stars;
		/// the color to display a collected star with
		public Color StarOnColor;
		/// the color to display a not collected star with
		public Color StarOffColor;

		/// <summary>
		/// On Update we update our star text
		/// </summary>
		protected virtual void Update()
		{
			UpdateStars ();
		}

		/// <summary>
		/// Every frame we update our star text with the current version
		/// </summary>
		protected virtual void UpdateStars()
		{
			StarDisplayText.text = RetroAdventureProgressManager.Instance.CurrentStars.ToString();
		}

		/// <summary>
		/// When the level is complete we display our level complete splash and set its values
		/// </summary>
		public virtual void LevelComplete()
		{
			if (Inventories != null)
			{
				Inventories.SetActive (false);	
			}
			EventSystem.current.sendNavigationEvents=true;
			GameManager.Instance.Pause (PauseMethods.NoPauseMenu);



            //ARTALASKY     |   \   \   \   \   \
            //LevelCompleteSplash.SetActive(true);
            if(SceneManager.GetActiveScene().name == "Level 00")
	            Infrastructure.Instance.TrophyManager.UnlockAchievement("Level1");

            if(SceneManager.GetActiveScene().name == "Level 02")
	            Infrastructure.Instance.TrophyManager.UnlockAchievement("Level3");
            
            if(SceneManager.GetActiveScene().name == "Level 04")
	            Infrastructure.Instance.TrophyManager.UnlockAchievement("Level5");
            
            if(SceneManager.GetActiveScene().name == "Level 09")
	            Infrastructure.Instance.TrophyManager.UnlockAchievement("Level10");
            
            if (SceneManager.GetActiveScene().name == "Level 19")
            {
                LevelManager.Instance.SetNextLevel("Level 20");
            }
            LevelCompleteSplash.SetActive(true);
			

            //

            foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
			{
				if (scene.SceneName == SceneManager.GetActiveScene().name)
				{
					for (int i=0; i<Stars.Length; i++)
					{
						Stars [i].color = (scene.CollectedStars [i]) ? StarOnColor : StarOffColor;							
					}
				}
			}

			if (LevelCompleteSplashFocus != null)
			{
				EventSystem.current.SetSelectedGameObject(LevelCompleteSplashFocus, null);
			}
		}

		/// <summary>
		/// When grabbing a level complete event, we call our LevelComplete method
		/// </summary>
		/// <param name="corgiEngineEvent">Corgi engine event.</param>
		public virtual void OnMMEvent(CorgiEngineEvent corgiEngineEvent)
		{
			switch (corgiEngineEvent.EventType)
			{
				case CorgiEngineEventTypes.LevelComplete:
					LevelComplete ();
					break;
			}
		}

		protected virtual void OnEnable()
		{
			this.MMEventStartListening<CorgiEngineEvent>();
		}

		protected virtual void OnDisable()
		{
			this.MMEventStopListening<CorgiEngineEvent>();
		}
	}
}
