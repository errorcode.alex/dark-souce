﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTrigger : MonoBehaviour {

    public AudioSource _snd;
	// Use this for initialization
	void Start () {
        _snd = _snd.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) {
        if (col.tag == "Player")
        {
            _snd.enabled = true;
        }
	}

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            _snd.enabled = false;
        }
    }
}
