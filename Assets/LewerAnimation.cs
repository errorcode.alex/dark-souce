﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LewerAnimation : MonoBehaviour
{

    public Animator _myAnim;
    public string RunBoolTag; // Имя параметра в аниматоре запускающего анимацию открытия/закрытия
    public bool RunOnOff = true; //Какое значение установлено на старте

    // Use this for initialization
    void Start()
    {
        _myAnim = _myAnim.GetComponent<Animator>();
    }

    public void InteractMechanism()
    {
        // Анимация рычага
        if (_myAnim.GetBool(RunBoolTag) == false)
        { _myAnim.SetBool(RunBoolTag, true); }
        else
        { _myAnim.SetBool(RunBoolTag, false); }
    }
}
