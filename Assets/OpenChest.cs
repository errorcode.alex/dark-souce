﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public class OpenChest : MonoBehaviour
    {

        public GameObject PrefabForSpawn;
        public Animator anim;
        public Vector3 SpawnDestination;
        public float SpawnSpeed = 0.2f;
        public bool AnimateSpawn = true;
        public bool Used;


        protected virtual void Initialization()
        {  
            anim = anim.GetComponent<Animator>();
        }

        public void OpeningChest()
        {
            if (Used == false)
            {
                Used = true;
                anim.SetTrigger("Open"); // Проиграть анимацию
                                         // Проиграть звук открытия

                GameObject spawned = Instantiate(PrefabForSpawn, transform.position, Quaternion.identity); // Заспавнить объект

                spawned.transform.position = transform.position;
                spawned.transform.rotation = Quaternion.identity;

                if (AnimateSpawn)
                {
                    StartCoroutine(MMMovement.MoveFromTo(spawned, transform.position, new Vector2(transform.position.x + SpawnDestination.x, transform.position.y + GetComponent<BoxCollider2D>().size.y + SpawnDestination.y), SpawnSpeed, 0.05f));
                }
                else
                {
                    spawned.transform.position = transform.position + SpawnDestination;
                }

                
            }
        }


    }
}